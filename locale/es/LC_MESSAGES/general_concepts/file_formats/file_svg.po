# Spanish translations for docs_krita_org_general_concepts___file_formats___file_svg.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_svg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-03 23:05+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../general_concepts/file_formats/file_svg.rst:1
msgid "The Scalable Vector Graphics file format in Krita."
msgstr "El formato de archivo de gráficos vectoriales escalables de Krita."

#: ../../general_concepts/file_formats/file_svg.rst:10
msgid "SVG"
msgstr ""

#: ../../general_concepts/file_formats/file_svg.rst:10
#, fuzzy
#| msgid "\\*.svg"
msgid "*.svg"
msgstr "\\*.svg"

#: ../../general_concepts/file_formats/file_svg.rst:10
#, fuzzy
#| msgid "The Scalable Vector Graphics file format in Krita."
msgid "Scalable Vector Graphics Format"
msgstr "El formato de archivo de gráficos vectoriales escalables de Krita."

#: ../../general_concepts/file_formats/file_svg.rst:15
msgid "\\*.svg"
msgstr "\\*.svg"

#: ../../general_concepts/file_formats/file_svg.rst:17
msgid ""
".svg, or Scalable Vector Graphics, is the most modern vector graphics "
"interchange file format out there."
msgstr ""
".svg o Gráficos Vectoriales Escalables es el formato de archivo de "
"intercambio de gráficos vectoriales más moderno que existe."

#: ../../general_concepts/file_formats/file_svg.rst:19
msgid ""
"Being vector graphics, svg is very light weight. This is because it usually "
"only stores coordinates and parameters for the maths involved with vector "
"graphics."
msgstr ""
"Al contener un gráfico vectorial, svg es muy ligero. Esto se debe a que solo "
"suele contener coordenadas y parámetros para las operaciones matemáticas "
"involucradas en los gráficos vectoriales."

#: ../../general_concepts/file_formats/file_svg.rst:21
msgid ""
"It is maintained by the w3c svg working group, who also maintain other open "
"standards that make up our modern internet."
msgstr ""
"Lo mantiene el grupo de trabajo w3c svg, que también mantiene otros "
"estándares abiertos que constituyen nuestra moderna Internet."

#: ../../general_concepts/file_formats/file_svg.rst:23
msgid ""
"While you can open up svg files with any text-editor to edit them, it is "
"best to use a vector program like Inkscape. Krita 2.9 to 3.3 supports "
"importing svg via the add shape docker. Since Krita 4.0, SVGs can be "
"properly imported, and you can export singlevector layers via :menuselection:"
"`Layer --> Import/Export --> Save Vector Layer as SVG...`. For 4.0, Krita "
"will also use SVG to save vector data into its :ref:`internal format "
"<file_kra>`."
msgstr ""
"Aunque es posible abrir archivos svg con cualquier editor de texto para "
"modificarlos, lo mejor es usar un programa vectorial, como Inkscape. Krita "
"2.9 a 3.3 permite importar svg mediante el panel para añadir formas. A "
"partir de Krita 4.0, se puede importar SVG correctamente, además de poder "
"exportar capas vectoriales sencillas mediante :menuselection:`Capa --> "
"Importar/Exportar --> Guardar capa vectorial como SVG...`. Para Krita 4.0 "
"también se usará SVG para guardar datos vectoriales en su :ref:`formato "
"interno <file_kra>`."

#: ../../general_concepts/file_formats/file_svg.rst:25
msgid ""
"svg is designed for the internet, though sadly, because vector graphics are "
"considered a bit obscure compared to raster graphics, not a lot of websites "
"accept them yet. Hosting them on your own webhost works just fine though."
msgstr ""
