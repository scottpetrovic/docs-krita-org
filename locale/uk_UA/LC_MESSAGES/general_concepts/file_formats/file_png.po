# Translation of docs_krita_org_general_concepts___file_formats___file_png.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_png\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 09:47+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../general_concepts/file_formats/file_png.rst:1
msgid "The Portable Network Graphics file format in Krita."
msgstr "Формат файлів Portable Network Graphics у Krita."

#: ../../general_concepts/file_formats/file_png.rst:11
msgid "*.png"
msgstr "*.png"

#: ../../general_concepts/file_formats/file_png.rst:11
msgid "png"
msgstr "png"

#: ../../general_concepts/file_formats/file_png.rst:11
msgid "portable network graphics"
msgstr "portable network graphics"

#: ../../general_concepts/file_formats/file_png.rst:17
msgid "\\*.png"
msgstr "\\*.png"

#: ../../general_concepts/file_formats/file_png.rst:19
msgid ""
".png, or Portable Network Graphics, is a modern alternative to :ref:"
"`file_gif` and with that and :ref:`file_jpg` it makes up the three main "
"formats that are widely supported on the internet."
msgstr ""
".png або Portable Network Graphics (портативна графіка для мережі) — сучасна "
"альтернатива :ref:`file_gif`, а також :ref:`file_jpg`. Це третій з основних "
"форматів, які мають широку підтримку в інтернеті."

#: ../../general_concepts/file_formats/file_png.rst:21
msgid ""
"png is a :ref:`lossless <lossless_compression>` file format, which means "
"that it is able to maintain all the colors of your image perfectly. It does "
"so at the cost of the file size being big, and therefore it is recommended "
"to try :ref:`file_jpg` for images with a lot of gradients and different "
"colors. Grayscale images will do better in png as well as images with a lot "
"of text and sharp contrasts, like comics."
msgstr ""
"png є форматом зберігання даних :ref:`без втрат <lossless_compression>`. Це "
"означає, що у форматі можна ідеально відтворювати усі кольори зображення. "
"Зберігання без втрат робить розмір файлів доволі великим, тому рекомендуємо "
"використовувати :ref:`file_jpg` для зображень із багатьма градієнтами і "
"різними кольорами. У форматі PNG варто зберігати зображення у відтінках "
"сірого кольору, а також зображення із великими фрагментами тексту та різкими "
"контрастами, зокрема комікси."

#: ../../general_concepts/file_formats/file_png.rst:23
msgid ""
"Like :ref:`file_gif`, png can support indexed color. Unlike :ref:`file_gif`, "
"png doesn't support animation. There have been two attempts at giving "
"animation support to png, apng and mng, the former is unofficial and the "
"latter too complicated, so neither have really taken off yet."
msgstr ""
"Подібно до :ref:`file_gif`, у png передбачено підтримку індексованих "
"кольорів. На відміну від :ref:`file_gif`, у png не передбачено підтримки "
"анімації. Було дві спроби реалізувати підтримку анімації у png, apng та mng. "
"Перша з них була неофіційною, друга — надто складною, тому жоден із них так "
"і не набув популярності."

#: ../../general_concepts/file_formats/file_png.rst:25
msgid ""
"Since 4.2 we support saving HDR to PNG as according to the `W3C PQ HDR PNG "
"standard <https://www.w3.org/TR/png-hdr-pq/>`_. To save as such files, "
"toggle :guilabel:`Save as HDR image (Rec. 2020 PQ)`, which will convert your "
"image to the Rec 2020 PQ color space and then save it as a special HDR png."
msgstr ""
"Починаючи з версії 4.2, передбачено підтримку збереження даних із широким "
"динамічним діапазоном до PNG відповідно до `стандарту W3C PQ HDR PNG "
"<https://www.w3.org/TR/png-hdr-pq/>`_. Щоб зберегти файли у цьому форматі, "
"позначте пункт :guilabel:`Зберегти як зображення HDR (Rec. 2020 PQ)`. У "
"результаті зображення буде перетворено до простору кольорів Rec 2020 PQ, а "
"потім збережено до спеціалізованого файла PNG із HDR."
