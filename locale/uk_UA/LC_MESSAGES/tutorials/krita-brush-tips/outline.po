# Translation of docs_krita_org_tutorials___krita-brush-tips___outline.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_tutorials___krita-brush-tips___outline\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 13:33+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../tutorials/krita-brush-tips/outline.rst:None
msgid ""
".. image:: images/brush-tips/Krita-layerstyle_hack.png\n"
"   :alt: image demonstrating the layer style hack for this effect"
msgstr ""
".. image:: images/brush-tips/Krita-layerstyle_hack.png\n"
"   :alt: Демонстрація на зображенні зміни стилю шару для отримання цього "
"ефекту"

#: ../../tutorials/krita-brush-tips/outline.rst:None
msgid ""
".. image:: images/brush-tips/Krita-layerstyle_hack2.png\n"
"   :alt: image demonstrating the layer style hack for this effect"
msgstr ""
"2.. image:: images/brush-tips/Krita-layerstyle_hack2.png\n"
"   :alt: Демонстрація на зображенні зміни стилю шару для отримання цього "
"ефекту"

#: ../../tutorials/krita-brush-tips/outline.rst:1
msgid "A tutorial about painting outline while you draw with brush"
msgstr "Підручник щодо малювання контуру під час малювання пензлем"

#: ../../tutorials/krita-brush-tips/outline.rst:13
msgid "Brush-tips:Outline"
msgstr "Кінчики пензлів: Контур"

#: ../../tutorials/krita-brush-tips/outline.rst:16
msgid "Question"
msgstr "Питання"

#: ../../tutorials/krita-brush-tips/outline.rst:18
msgid "How to make an outline for a single brush stroke using Krita?"
msgstr "Як створити контур окремого мазка пензлем за допомогою Krita?"

#: ../../tutorials/krita-brush-tips/outline.rst:20
msgid ""
"Not really a brush, but what you can do is add a layer style to a layer, by |"
"mouseright| a layer and selecting layer style. Then input the following "
"settings:"
msgstr ""
"Це не зовсім малювання пензлем, але ви можете додати до шару стиль. Клацніть "
"|mouseright| на пункті шару і виберіть пункт стилю шару. Далі вкажіть такі "
"параметри:"

#: ../../tutorials/krita-brush-tips/outline.rst:25
msgid ""
"Then, set the main layer to multiply (or add a :ref:`filter_color_to_alpha` "
"filter mask), and paint with white:"
msgstr ""
"Далі, встановіть для основного шару режим змішування «Множення» (або додайте "
"маску фільтрування :ref:`filter_color_to_alpha`) і малюйте білим кольором:"

#: ../../tutorials/krita-brush-tips/outline.rst:30
msgid ""
"(The white thing is the pop-up that you see as you hover over the layer)"
msgstr ""
"(Біла штука — контекстна панель, яку ви бачите, коли наводите вказівник миші "
"на пункт шару)"

#: ../../tutorials/krita-brush-tips/outline.rst:32
msgid "Merge into a empty clear layer after ward to fix all the effects."
msgstr ""
"Далі, виконайте об'єднання до порожнього чистого шару, щоб зафіксувати усі "
"ефекти."
