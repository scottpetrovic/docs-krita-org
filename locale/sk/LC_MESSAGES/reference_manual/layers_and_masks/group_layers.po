# translation of docs_krita_org_reference_manual___layers_and_masks___group_layers.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___layers_and_masks___group_layers\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 10:40+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/layers_and_masks/group_layers.rst:1
msgid "How to use group layers in Krita."
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
#, fuzzy
#| msgid "Group Layers"
msgid "Layers"
msgstr "Skupinové vrstvy"

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
#, fuzzy
#| msgid "Group Layers"
msgid "Groups"
msgstr "Skupinové vrstvy"

#: ../../reference_manual/layers_and_masks/group_layers.rst:12
msgid "Passthrough Mode"
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:17
msgid "Group Layers"
msgstr "Skupinové vrstvy"

#: ../../reference_manual/layers_and_masks/group_layers.rst:19
msgid ""
"While working in complex artwork you'll often find the need to group the "
"layers or some portions and elements of the artwork in one unit. Group "
"layers come in handy for this, they allow you to make a segregate the "
"layers, so you can hide these quickly, or so you can apply a mask to all the "
"layers inside this group as if they are one, you can also recursively "
"transform the content of the group... Just drag the mask so it moves to the "
"layer. They are quickly made with :kbd:`Ctrl + G`."
msgstr ""

#: ../../reference_manual/layers_and_masks/group_layers.rst:21
msgid ""
"A thing to note is that the layers inside a group layer are considered "
"separately when the layer gets composited, the layers inside a group are "
"separately composited and then this image is taken in to account when "
"compositing the whole image, while on the contrary, the groups in Photoshop "
"have something called pass-through mode which makes the layer behave as if "
"they are not in a group and get composited along with other layers of the "
"stack. The recent versions of Krita have pass-through mode you can enable it "
"to get similar behavior"
msgstr ""
