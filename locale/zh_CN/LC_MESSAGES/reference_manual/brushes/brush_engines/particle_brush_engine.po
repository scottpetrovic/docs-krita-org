msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-05 22:27\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines___particle_brush_engine."
"pot\n"

#: ../../<generated>:1
msgid "Iterations"
msgstr "迭代"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:1
msgid "The Particle Brush Engine manual page."
msgstr "介绍 Krita 的粒子笔刷引擎。"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:16
msgid "Particle Brush Engine"
msgstr "粒子笔刷引擎"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:11
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:19
msgid ".. image:: images/icons/particlebrush.svg"
msgstr ".. image:: images/icons/particlebrush.svg"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:20
msgid ""
"A brush that draws wires using parameters. These wires always get more "
"random and crazy over drawing distance. Gives very intricate lines best used "
"for special effects."
msgstr ""
"尽管名字是“粒子”，但这个笔刷引擎并非画出粒子，而是按照粒子的流动原理画出粒子"
"的流线轨迹。效果根据笔画的走向和距离变化多端，适合用于添加特效。"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:23
msgid "Options"
msgstr "可用笔刷选项"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:24
msgid ":ref:`option_size_particle`"
msgstr ":ref:`option_size_particle`"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:25
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:26
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:27
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:32
msgid "Brush Size"
msgstr "笔刷大小"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:34
msgid "Particles"
msgstr "粒子"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:35
msgid "How many particles there's drawn."
msgstr "控制在画布上划出流动轨迹的粒子数目。"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:36
msgid "Opacity Weight"
msgstr "不透明度权重"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:37
msgid "The Opacity of all particles. Is influenced by the painting mode."
msgstr ""
"每个粒子轨迹的不透明度，效果受绘画模式的影响。在堆积模式下，散开的轨迹比较透"
"明，重合的轨迹比较不透明；在冲刷模式下，散开的和重合的轨迹的透明度是一致的。"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:38
msgid "Dx Scale (Distance X Scale)"
msgstr "距离 X 比例"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:39
msgid ""
"How much the horizontal cursor distance affects the placing of the pixel. Is "
"unstable on negative values. 1.0 is equal."
msgstr "光标的水平移动距离对像素绘制位置的影响比例。负值时不稳定。1.0 为相等。"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:40
msgid "Dy Scale (Distance Y Scale)"
msgstr "距离 Y 比例"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:41
msgid ""
"How much the vertical cursor distance affects the placing of the pixel. Is "
"unstable on negative values. 1.0 is equal."
msgstr "光标的垂直移动距离对像素绘制位置的影响比例。负值时不稳定。1.0 为相等。"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:42
msgid "Gravity"
msgstr "重力"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:43
msgid ""
"Multiplies with the previous particle's position, to find the new particle's "
"position."
msgstr "此数值会与粒子之前位置相乘，得出粒子的新位置。"

#: ../../reference_manual/brushes/brush_engines/particle_brush_engine.rst:45
msgid ""
"The higher, the higher the internal acceleration is, with the furthest away "
"particle from the brush having the highest acceleration. This means that the "
"higher iteration is, the faster and more randomly a particle moves over "
"time, giving a messier result."
msgstr ""
"迭代数值越大，内部加速率就越大。这意味着粒子离笔刷越远，加速率就越大，粒子的"
"移动速度和随机度都会随着粒子的移动时间增加，画出的笔画也更加多变。"
