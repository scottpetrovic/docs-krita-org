msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-05 22:27\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_engines___spray_brush_engine."
"pot\n"

#: ../../<generated>:1
msgid "Mix with background color."
msgstr "与背景色混合"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:1
msgid "The Spray Brush Engine manual page."
msgstr "介绍 Krita 的喷雾笔刷引擎。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:16
msgid "Spray Brush Engine"
msgstr "喷雾笔刷引擎"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:11
msgid "Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:11
msgid "Airbrush"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:19
msgid ".. image:: images/icons/spraybrush.svg"
msgstr ".. image:: images/icons/spraybrush.svg"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:20
msgid "A brush that can spray particles around in its brush area."
msgstr "这个笔刷引擎可以在笔刷范围内喷射粒子。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:23
msgid "Options"
msgstr "可用笔刷选项"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:25
msgid ":ref:`option_spray_area`"
msgstr ":ref:`option_spray_area`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:26
msgid ":ref:`option_spray_shape`"
msgstr ":ref:`option_spray_shape`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:27
msgid ":ref:`option_brush_tip` (Used as particle if spray shape is not active)"
msgstr ""
":ref:`option_brush_tip` (如果“喷雾形状”没有启用，则使用笔尖作为喷雾形状)"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:28
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:29
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:30
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:31
msgid ":ref:`option_shape_dyna`"
msgstr ":ref:`option_shape_dyna`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:32
msgid ":ref:`option_color_spray`"
msgstr ":ref:`option_color_spray`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:33
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:34
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:39
msgid "Spray Area"
msgstr "喷雾区域"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:41
msgid "The area in which the particles are sprayed."
msgstr "此选项页面控制喷雾的区域。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:43
msgid "Diameter"
msgstr "直径"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:44
msgid "The size of the area."
msgstr "喷雾区域的直径大小。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:45
msgid "Aspect Ratio"
msgstr "宽高比"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:46
msgid "It's aspect ratio: 1.0 is fully circular."
msgstr "喷雾椭圆区域的宽高比，1.0 为正圆形。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:47
msgid "Angle"
msgstr "角度"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:48
msgid ""
"The angle of the spray size: works nice with aspect ratios other than 1.0."
msgstr "喷雾椭圆区域的角度，在宽高比不是 1.0 是有效。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:49
msgid "Scale"
msgstr "缩放"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:50
msgid "Scales the diameter up."
msgstr "对直径数值进行缩放。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:52
msgid "Spacing"
msgstr "间距"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:52
msgid "Increases the spacing of the diameter's spray."
msgstr "控制每次喷雾区域的间距。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:55
msgid "Particles"
msgstr "粒子"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:57
msgid "Count"
msgstr "数量"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:58
msgid "Use a specified amount of particles."
msgstr "指定喷雾的颗粒数量。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:59
msgid "Density"
msgstr "密度"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:60
msgid "Use a % amount of particles."
msgstr "按百分比控制粒子数量。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:61
msgid "Jitter Movement"
msgstr "抖动位移"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:62
msgid "Jitters the spray area around for extra randomness."
msgstr "向各个方向抖动喷雾区域以增加随机度。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:64
msgid "Gaussian Distribution"
msgstr "高斯分布"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:64
msgid ""
"Focuses the particles to paint in the center instead of evenly random over "
"the spray area."
msgstr "将粒子集中在喷雾区域的中间附近，而不是随机喷射在区域内。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:69
msgid "Spray Shape"
msgstr "喷雾形状"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:71
msgid ""
"If activated, this will generate a special particle. If not, the brush-tip "
"will be the particle."
msgstr ""
"勾选此选项则按照此页配置生成特有的粒子，不勾选此选项将使用笔尖作为粒子。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:74
msgid "Can be..."
msgstr "可用形状如下："

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:76
msgid "Ellipse"
msgstr "椭圆形"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:77
msgid "Rectangle"
msgstr "矩形"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:78
msgid "Anti-aliased Pixel"
msgstr "反锯齿像素"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:79
msgid "Pixel"
msgstr "像素"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:80
msgid "Shape"
msgstr "形状"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:80
msgid "Image"
msgstr "图像"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:82
msgid "Width & Height"
msgstr "宽度和高度"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:83
msgid "Decides the width and height of the particle."
msgstr "控制粒子的宽度和高度。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:84
msgid "Proportional"
msgstr "使用比例数值"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:85
msgid "Locks Width & Height to be the same."
msgstr "为宽度和高度数值使用喷雾区域大小的百分比。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:87
msgid "Texture"
msgstr "纹理"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:87
msgid "Allows you to pick an image for the :guilabel:`Image shape`."
msgstr "可以载入一张图像用于 :guilabel:`图像` 喷雾形状。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:92
msgid "Shape Dynamics"
msgstr "形状动态"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:94
msgid "Random Size"
msgstr "随机大小"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:95
msgid ""
"Randomizes the particle size between 1x1 px and the given size of the "
"particle in brush-tip or spray shape."
msgstr "在 1x1 px 和笔尖、喷雾形状中指定的大小之间随机变化粒子的大小。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:96
msgid "Fixed Rotation"
msgstr "固定旋转"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:97
msgid "Gives a fixed rotation to the particle to work from."
msgstr "让粒子旋转到一个固定的角度。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:98
msgid "Randomized Rotation"
msgstr "随机旋转"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:99
msgid "Randomizes the rotation."
msgstr "随机旋转粒子的角度。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:100
msgid "Follow Cursor Weight"
msgstr "跟随光标权重"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:101
msgid ""
"How much the pressure affects the rotation of the particles. At 1.0 and high "
"pressure it'll seem as if the particles are exploding from the middle."
msgstr ""
"控制压力影响粒子旋转的程度。此数值为 1.0 时使用高压力将使得粒子看似从光标中间"
"朝四面八方喷出。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:103
msgid "Angle Weight"
msgstr "角度权重"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:103
msgid "How much the spray area angle affects the particle angle."
msgstr "控制喷雾区域的角度对粒子角度的影响程度。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:108
msgid "Color Options"
msgstr "颜色选项"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:110
msgid "Random HSV"
msgstr "随机 HSV"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:111
msgid ""
"Randomize the HSV with the strength of the sliders. The higher, the more the "
"color will deviate from the foreground color, with the direction indicating "
"clock or counter clockwise."
msgstr ""
"按照滑动条的程度对 HSV 数值进行随机变化。数值越大，偏离前景色的程度就越大。正"
"值为色相环顺时针角度范围，负值为色相关逆时针角度范围。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:112
msgid "Random Opacity"
msgstr "随机不透明度"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:113
msgid "Randomizes the opacity."
msgstr "对不透明度进行随机变化。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:114
msgid "Color Per Particle"
msgstr "按粒子着色"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:115
msgid "Has the color options be per particle instead of area."
msgstr "使颜色选项对每个粒子生效，而不是对整个粒子区域生效。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:116
msgid "Sample Input Layer."
msgstr "采样输入图层"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:117
msgid ""
"Will use the underlying layer as reference for the colors instead of the "
"foreground color."
msgstr "用下方图层的颜色作为颜色变化的参考，而不是前景色。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:118
msgid "Fill Background"
msgstr "填充背景"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:119
msgid "Fills the area before drawing the particles with the background color."
msgstr "在绘制粒子之前，先用背景色填充粒子区域。"

#: ../../reference_manual/brushes/brush_engines/spray_brush_engine.rst:121
msgid ""
"Gives the particle a random color between foreground/input/random HSV and "
"the background color."
msgstr "粒子颜色将在前景色、输入、随机 HSV 数值、背景色之间随机变化。"
