msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-05 22:27\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___tools___ellipse.pot\n"

#: ../../<rst_epilog>:28
msgid ""
".. image:: images/icons/ellipse_tool.svg\n"
"   :alt: toolellipse"
msgstr ""
".. image:: images/icons/ellipse_tool.svg\n"
"   :alt: ellipse"

#: ../../reference_manual/tools/ellipse.rst:None
msgid ".. image:: images/tools/Krita_ellipse_circle.gif"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:None
msgid ".. image:: images/tools/Krita_ellipse_from_center.gif"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:None
msgid ".. image:: images/tools/Krita_ellipse_reposition.gif"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:1
msgid "Krita's ellipse tool reference."
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:11
msgid "Ellipse"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:11
msgid "Circle"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:16
msgid "Ellipse Tool"
msgstr "椭圆工具"

#: ../../reference_manual/tools/ellipse.rst:18
msgid "|toolellipse|"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:20
msgid ""
"Use this tool to paint an ellipse. The currently selected brush is used for "
"drawing the ellipse outline. Click and hold the left mouse button to "
"indicate one corner of the ‘bounding rectangle’ of the ellipse, then move "
"your mouse to the opposite corner. :program:`Krita` will show a preview of "
"the ellipse using a thin line. Release the button to draw the ellipse."
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:22
msgid ""
"While dragging the ellipse, you can use different modifiers to control the "
"size and position of your ellipse:"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:24
msgid ""
"In order to make a circle instead of an ellipse, hold :kbd:`Shift` while "
"dragging. After releasing :kbd:`Shift` any movement of the mouse will give "
"you an ellipse again:"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:29
msgid ""
"In order to keep the center of the ellipse fixed and only growing and "
"shrinking the ellipse around it, hold :kbd:`Ctrl` while dragging:"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:34
msgid "In order to move the ellipse around, hold :kbd:`Alt`:"
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:39
msgid ""
"You can change between the corner/corner and center/corner dragging methods "
"as often as you want by holding down or releasing :kbd:`Ctrl`, provided you "
"keep the left mouse button pressed. With :kbd:`Ctrl` pressed, mouse "
"movements will affect all four corners of the bounding rectangle (relative "
"to the center), without :kbd:`Ctrl`, the corner opposite to the one you are "
"moving remains still. With :kbd:`Alt` pressed, all four corners will be "
"affected, but the size stays the same."
msgstr ""

#: ../../reference_manual/tools/ellipse.rst:42
msgid "Tool Options"
msgstr "工具选项"
