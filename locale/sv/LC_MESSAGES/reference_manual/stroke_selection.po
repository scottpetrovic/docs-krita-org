# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:22+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/Stroke_Selection_4.png"
msgstr ".. image:: images/Stroke_Selection_4.png"

#: ../../reference_manual/stroke_selection.rst:1
msgid "How to use the stroke selection command in Krita."
msgstr "Hur dragmarkeringskommandot används i Krita."

#: ../../reference_manual/stroke_selection.rst:10
msgid "Selection"
msgstr "Markering"

#: ../../reference_manual/stroke_selection.rst:10
msgid "Stroke"
msgstr "Drag"

#: ../../reference_manual/stroke_selection.rst:15
msgid "Stroke Selection"
msgstr "Dragmarkering"

#: ../../reference_manual/stroke_selection.rst:17
msgid ""
"Sometimes, you want to add an even border around a selection. Stroke "
"Selection allows you to do this. It's under :menuselection:`Edit --> Stroke "
"Selection`"
msgstr ""
"Ibland vill man lägga till en jämn kant omkring en markering. Dragmarkering "
"låter dig göra det. Den finns under :menuselection:`Redigera --> "
"Dragmarkering`."

#: ../../reference_manual/stroke_selection.rst:19
msgid "First make a selection and call up the menu:"
msgstr "Gör först en markering och visa menyn:"

#: ../../reference_manual/stroke_selection.rst:22
msgid ".. image:: images/Krita_stroke_selection_1.png"
msgstr ".. image:: images/Krita_stroke_selection_1.png"

#: ../../reference_manual/stroke_selection.rst:23
msgid ""
"The main options are about using the current brush, or lining the selection "
"with an even line. You can use the current foreground color, the background "
"color or a custom color."
msgstr ""
"Huvudalternativen gäller använda aktuell pensel, eller omge markeringen med "
"en jämn linje. Man kan använda aktuell förgrundsfärg, bakgrundsfärg eller en "
"egen färg."

#: ../../reference_manual/stroke_selection.rst:25
msgid "Using the current brush allows you to use textured brushes:"
msgstr "Genom att använda aktuell pensel kan strukturpenslar användas:"

#: ../../reference_manual/stroke_selection.rst:28
msgid ".. image:: images/Stroke_selection_2.png"
msgstr ".. image:: images/Stroke_selection_2.png"

#: ../../reference_manual/stroke_selection.rst:29
msgid ""
"Lining the selection also allows you to set the background color, on top of "
"the line width in pixels or inches:"
msgstr ""
"Att omge markeringen låter dig också ange bakgrundsfärgen, förutom "
"linjebredd i bildpunkter eller tum:"

#: ../../reference_manual/stroke_selection.rst:32
msgid ".. image:: images/Krita_stroke_selection_3.png"
msgstr ".. image:: images/Krita_stroke_selection_3.png"

#: ../../reference_manual/stroke_selection.rst:33
msgid "This creates nice silhouettes:"
msgstr "Det skapar snygga siluetter:"
