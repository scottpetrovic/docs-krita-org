# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-05 09:23+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:1
msgid "Guide on all the specifics of creating Krita python plugins."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:14
msgid "Python"
msgstr "Python"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:14
msgid "Python Scripting"
msgstr "Skript med Python"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:14
msgid "Scripting"
msgstr "Skript"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:14
msgid "Plugin"
msgstr "Insticksprogram"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:19
msgid "How to make a Krita Python plugin"
msgstr "Hur man skapar ett Krita Python-insticksprogram"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:21
msgid ""
"You might have some neat scripts you have written in the Scripter Python "
"runner, but maybe you want to do more with it and run it automatically for "
"instance. Wrapping your script in a plugin can give you much more "
"flexibility and power than running scripts from the Scripter editor."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:23
msgid ""
"Okay, so even if you know python really well, there are some little details "
"to getting Krita to recognize a python plugin. So this page will give an "
"overview how to create the various types of python script unique to Krita."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:25
msgid ""
"These mini-tutorials are written for people with a basic understanding of "
"python, and in such a way to encourage experimentation instead of plainly "
"copy and pasting code, so read the text carefully."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:28
msgid "Getting Krita to recognize your plugin"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:30
msgid ""
"A script in Krita has two components - the script directory (holding your "
"script's Python files) and a \".desktop\" file that Krita uses to load and "
"register your script. For Krita to load your script both of these must put "
"be in the pykrita subdirectory of your Krita resources folder (on Linux  ~/."
"local/share/krita/pykrita). To find your resources folder start Krita and "
"click the :menuselection:`Settings --> Manage Resources` menu item. This "
"will open a dialog box. Click the :guilabel:`Open Resources Folder` button. "
"This should open a file manager on your system at your Krita resources "
"folder. See the `API <https://api.kde.org/extragear-api/graphics-apidocs/"
"krita/libs/libkis/html/index.html>`_ docs under \"Auto starting scripts\".  "
"If there is no pykrita subfolder in the Krita resources directory use your "
"file manager to create one."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:32
msgid ""
"Scripts are identified by a file that ends in a .desktop extension that "
"contain information about the script itself."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:34
msgid ""
"Therefore, for each proper plugin you will need to create a folder, and a "
"desktop file."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:36
msgid "The desktop file should look as follows::"
msgstr "Skrivbordsfilen ska se ut på följande sätt:"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:38
msgid ""
"[Desktop Entry]\n"
"Type=Service\n"
"ServiceTypes=Krita/PythonPlugin\n"
"X-KDE-Library=myplugin\n"
"X-Python-2-Compatible=false\n"
"X-Krita-Manual=myPluginManual.html\n"
"Name=My Own Plugin\n"
"Comment=Our very own plugin."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:47
msgid "Type"
msgstr "Type"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:48
msgid "This should always be service."
msgstr "Det ska alltid vara service."

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:49
msgid "ServiceTypes"
msgstr "ServiceTypes"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:50
msgid "This should always be Krita/PythonPlugin for python plugins."
msgstr "Det ska alltid vara Krita/PythonPlugin för Python-insticksprogram."

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:51
msgid "X-KDE-Library"
msgstr "X-KDE-Library"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:52
msgid "This should be the name of the plugin folder you just created."
msgstr ""
"Det ska vara namnet på katalogen för insticksprogrammet som du just skapade."

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:53
msgid "X-Python-2-Compatible"
msgstr "X-Python-2-Compatible"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:54
msgid ""
"Whether it is python 2 compatible. If Krita was built with python 2 instead "
"of 3 (``-DENABLE_PYTHON_2=ON`` in the cmake configuration), then this plugin "
"will not show up in the list."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:55
msgid "X-Krita-Manual"
msgstr "X-Krita-Manual"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:56
msgid ""
"An Optional Value that will point to the manual item. This is shown in the "
"Python Plugin manager. If it's `an HTML file it'll be shown as rich text "
"<https://doc.qt.io/qt-5/richtext-html-subset.html>`_, if not, it'll be shown "
"as plain text."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:57
msgid "Name"
msgstr "Name"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:58
msgid "The name that will show up in the Python Plugin Manager."
msgstr "Namnet som visas i Python-instickshanteraren."

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:60
msgid "Comment"
msgstr "Comment"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:60
msgid "The description that will show up in the Python Plugin Manager."
msgstr "Beskrivningen som visas i Python-instickshanteraren."

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:62
msgid ""
"Krita python plugins need to be python modules, so make sure there's an "
"__init__.py script, containing something like..."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:68
#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:87
msgid "from .myplugin import *"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:69
msgid ""
"Where .myplugin is the name of the main file of your plugin. If you restart "
"Krita, it now should show this in the Python Plugin Manager in the settings, "
"but it will be grayed out, because there's no myplugin.py. If you hover over "
"disabled plugins, you can see the error with them."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:72
msgid "Summary"
msgstr "Sammanfattning"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:74
msgid "In summary, if you want to create a script called *myplugin*:"
msgstr ""
"För att sammanfatta, om du vill skapa ett skript som heter *insticksprogram*:"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:77
msgid "in your Krita *resources/pykrita* directory create"
msgstr "skapa följande i Kritas katalog *resources/pykrita*"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:77
msgid "a folder called *myplugin*"
msgstr "en katalog som heter *insticksprogram*"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:78
msgid "a file called *myplugin.desktop*"
msgstr "en fil som heter *insticksprogram.desktop*"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:80
msgid "in the *myplugin* folder create"
msgstr "skapa följande i katalogen *insticksprogram*"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:80
msgid "a file called *__init__.py*"
msgstr "en fil som heter *__init__.py*"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:81
msgid "a file called *myplugin.py*"
msgstr "en fil som heter *insticksprogram.py*"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:82
msgid "in the *__init__.py* file put this code:"
msgstr "lägg till följande kod i filen *__init__.py*:"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:88
msgid "in the desktop file put this code::"
msgstr "lägg till följande kod i skrivbordsfilen:"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:90
msgid ""
"[Desktop Entry]\n"
"Type=Service\n"
"ServiceTypes=Krita/PythonPlugin\n"
"X-KDE-Library=myplugin\n"
"X-Python-2-Compatible=false\n"
"Name=My Own Plugin\n"
"Comment=Our very own plugin."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:98
msgid "write your script in the ''myplugin/myplugin.py'' file."
msgstr "skriv skriptet i filen \"insticksprogram/insticksprogram.py\"."

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:101
msgid "Creating an extension"
msgstr "Skapa en utökning"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:103
msgid ""
"`Extensions <https://api.kde.org/extragear-api/graphics-apidocs/krita/libs/"
"libkis/html/classExtension.html>`_ are relatively simple python scripts that "
"run on Krita start. They are made by extending the Extension class, and the "
"most barebones extension looks like this:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:123
msgid ""
"from krita import *\n"
"\n"
"class MyExtension(Extension):\n"
"\n"
"    def __init__(self, parent):\n"
"        #This is initialising the parent, always  important when "
"subclassing.\n"
"        super().__init__(parent)\n"
"\n"
"    def setup(self):\n"
"        pass\n"
"\n"
"    def createActions(self, window):\n"
"        pass\n"
"\n"
"# And add the extension to Krita's list of extensions:\n"
"Krita.instance().addExtension(MyExtension(Krita.instance()))"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:124
msgid ""
"This code of course doesn't do anything. Typically, in createActions we add "
"actions to Krita, so we can access our script from the :guilabel:`Tools` "
"menu."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:126
msgid ""
"First, let's create an `action <https://api.kde.org/extragear-api/graphics-"
"apidocs/krita/libs/libkis/html/classAction.html>`_. We can do that easily "
"with `Window.createAction() <https://api.kde.org/extragear-api/graphics-"
"apidocs/krita/libs/libkis/html/classWindow."
"html#a72ec58e53844076c1461966c34a9115c>`_. Krita will call createActions for "
"every Window that is created and pass the right window object that we have "
"to use."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:128
msgid "So..."
msgstr "Alltså ..."

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:135
msgid ""
"def createActions(self, window):\n"
"    action = window.createAction(\"myAction\", \"My Script\", \"tools/scripts"
"\")"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:136
msgid "\"myAction\""
msgstr "\"mitt_alternativ\""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:137
msgid ""
"This should be replaced with a unique id that Krita will use to find the "
"action."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:139
msgid "\"My Script\""
msgstr "\"Mitt skript\""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:139
msgid "This is what will be visible in the tools menu."
msgstr "Det är vad som syns i verktygsmenyn."

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:141
msgid ""
"If you now restart Krita, you will have an action called \"My Script\". It "
"still doesn't do anything, because we haven't connected it to a script."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:143
msgid ""
"So, let's make a simple export document script. Add the following to the "
"extension class, make sure it is above where you add the extension to Krita:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:158
msgid ""
"def exportDocument(self):\n"
"    # Get the document:\n"
"    doc =  Krita.instance().activeDocument()\n"
"    # Saving a non-existent document causes crashes, so lets check for that "
"first.\n"
"    if doc is not None:\n"
"        # This calls up the save dialog. The save dialog returns a tuple.\n"
"        fileName = QFileDialog.getSaveFileName()[0]\n"
"        # And export the document to the fileName location.\n"
"        # InfoObject is a dictionary with specific export options, but when "
"we make an empty one Krita will use the export defaults.\n"
"        doc.exportImage(fileName, InfoObject())"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:159
msgid "And add the import for QFileDialog above with the imports:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:165
msgid ""
"from krita import *\n"
"from PyQt5.QtWidgets import QFileDialog"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:166
msgid "Then, to connect the action to the new export document:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:174
msgid ""
"def createActions(self, window):\n"
"    action = window.createAction(\"myAction\", \"My Script\")\n"
"    action.triggered.connect(self.exportDocument)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:175
msgid ""
"This is an example of a `signal/slot connection <https://doc.qt.io/qt-5/"
"signalsandslots.html>`_, which Qt applications like Krita use a lot. We'll "
"go over how to make our own signals and slots a bit later."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:177
msgid "Restart Krita and your new action ought to now export the document."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:180
msgid "Creating configurable keyboard shortcuts"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:182
msgid ""
"Now, your new action doesn't show up in :menuselection:`Settings --> "
"Configure Krita --> Keyboard Shortcuts`."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:184
msgid ""
"Krita, for various reasons, only adds actions to the shortcuts menu when "
"they are present in an .action file. The action file to get our action to be "
"added to shortcuts should look like this:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:208
msgid ""
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
"<ActionCollection version=\"2\" name=\"Scripts\">\n"
"    <Actions category=\"Scripts\">\n"
"        <text>My Scripts</text>\n"
"\n"
"        <Action name=\"myAction\">\n"
"        <icon></icon>\n"
"        <text>My Script</text>\n"
"        <whatsThis></whatsThis>\n"
"        <toolTip></toolTip>\n"
"        <iconText></iconText>\n"
"        <activationFlags>10000</activationFlags>\n"
"        <activationConditions>0</activationConditions>\n"
"        <shortcut>ctrl+alt+shift+p</shortcut>\n"
"        <isCheckable>false</isCheckable>\n"
"        <statusTip></statusTip>\n"
"        </Action>\n"
"    </Actions>\n"
"</ActionCollection>"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:209
msgid "<text>My Scripts</text>"
msgstr "<text>Mina skript</text>"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:210
msgid ""
"This will create a sub-category under scripts called \"My Scripts\" to add "
"your shortcuts to."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:211
msgid "name"
msgstr "name"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:212
msgid ""
"This should be the unique id you made for your action when creating it in "
"the setup of the extension."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:213
msgid "icon"
msgstr "icon"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:214
msgid ""
"the name of a possible icon. These will only show up on KDE plasma, because "
"Gnome and Windows users complained they look ugly."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:215
msgid "text"
msgstr "text"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:216
msgid "The text that it will show in the shortcut editor."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:217
msgid "whatsThis"
msgstr "whatsThis"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:218
msgid ""
"The text it will show when a Qt application specifically calls for 'what is "
"this', which is a help action."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:219
msgid "toolTip"
msgstr "toolTip"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:220
msgid "The tool tip, this will show up on hover-over."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:221
msgid "iconText"
msgstr "iconText"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:222
msgid ""
"The text it will show when displayed in a toolbar. So for example, \"Resize "
"Image to New Size\" could be shortened to \"Resize Image\" to save space, so "
"we'd put that in here."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:223
msgid "activationFlags"
msgstr "activationFlags"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:224
msgid "This determines when an action is disabled or not."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:225
msgid "activationConditions"
msgstr "activationConditions"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:226
msgid ""
"This determines activation conditions (e.g. activate only when selection is "
"editable). See `the code <https://cgit.kde.org/krita.git/tree/libs/ui/"
"kis_action.h#n76>`_ for examples."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:227
msgid "shortcut"
msgstr "genväg"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:228
msgid "Default shortcut."
msgstr "Standardgenväg."

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:229
msgid "isCheckable"
msgstr "isCheckable"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:230
msgid "Whether it is a checkbox or not."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:232
msgid "statusTip"
msgstr "statusTip"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:232
msgid "The status tip that is displayed on a status bar."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:234
msgid ""
"Save this file as \"myplugin.action\" where myplugin is the name of your "
"plugin. The action file should be saved, not in the pykrita resources "
"folder, but rather in a resources folder named \"actions\". (So, share/"
"pykrita is where the python plugins and desktop files go, and share/actions "
"is where the action files go) Restart Krita. The shortcut should now show up "
"in the shortcut action list."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:237
msgid "Creating a docker"
msgstr "Skapa en panel"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:239
msgid ""
"Creating a custom `docker <https://api.kde.org/extragear-api/graphics-"
"apidocs/krita/libs/libkis/html/classDockWidget.html>`_ is much like creating "
"an extension. Dockers are in some ways a little easier, but they also "
"require more use of widgets. This is the barebones docker code:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:256
msgid ""
"from PyQt5.QtWidgets import *\n"
"from krita import *\n"
"\n"
"class MyDocker(DockWidget):\n"
"\n"
"    def __init__(self):\n"
"        super().__init__()\n"
"        self.setWindowTitle(\"My Docker\")\n"
"\n"
"    def canvasChanged(self, canvas):\n"
"        pass\n"
"\n"
"Krita.instance().addDockWidgetFactory(DockWidgetFactory(\"myDocker\", "
"DockWidgetFactoryBase.DockRight, MyDocker))"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:257
msgid ""
"The window title is how it will appear in the docker list in Krita. "
"canvasChanged always needs to be present, but you don't have to do anything "
"with it, so hence just 'pass'."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:259
msgid "For the addDockWidgetFactory..."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:261
msgid "\"myDocker\""
msgstr "\"myDocker\""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:262
msgid ""
"Replace this with an unique ID for your docker that Krita uses to keep track "
"of it."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:263
msgid "DockWidgetFactoryBase.DockRight"
msgstr "DockWidgetFactoryBase.DockRight"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:264
msgid ""
"The location. These can be DockTornOff, DockTop, DockBottom, DockRight, "
"DockLeft, or DockMinimized"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:266
msgid "MyDocker"
msgstr "MinPanel"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:266
msgid "Replace this with the class name of the docker you want to add."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:268
msgid ""
"So, if we add our export document function we created in the extension "
"section to this docker code, how do we allow the user to activate it? First, "
"we'll need to do some Qt GUI coding: Let's add a button!"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:270
msgid ""
"By default, Krita uses PyQt, but its documentation is pretty bad, mostly "
"because the regular Qt documentation is really good, and you'll often find "
"that the PyQT documentation of a class, say, `QWidget <https://www."
"riverbankcomputing.com/static/Docs/PyQt4/qwidget.html>`_ is like a weird "
"copy of the regular `Qt documentation <https://doc.qt.io/qt-5/qwidget."
"html>`_ for that class."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:272
msgid ""
"Anyway, what we need to do first is that we need to create a QWidget, it's "
"not very complicated, under setWindowTitle, add:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:278
msgid ""
"mainWidget = QWidget(self)\n"
"self.setWidget(mainWidget)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:279
msgid "Then, we create a button:"
msgstr "Därefter skapar vi en knapp:"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:284
msgid "buttonExportDocument = QPushButton(\"Export Document\", mainWidget)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:285
msgid ""
"Now, to connect the button to our function, we'll need to look at the "
"signals in the documentation. `QPushButton <https://doc.qt.io/qt-5/"
"qpushbutton.html>`_ has no unique signals of its own, but it does say it "
"inherits 4 signals from `QAbstractButton <https://doc.qt.io/qt-5/"
"qabstractbutton.html#signals>`_, which means that we can use those too. In "
"our case, we want clicked."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:290
msgid "buttonExportDocument.clicked.connect(self.exportDocument)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:291
msgid ""
"If we now restart Krita, we'll have a new docker and in that docker there's "
"a button. Clicking on the button will call up the export function."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:293
msgid ""
"However, the button looks aligned a bit oddly. That's because our mainWidget "
"has no layout. Let's quickly do that:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:299
msgid ""
"mainWidget.setLayout(QVBoxLayout())\n"
"mainWidget.layout().addWidget(buttonExportDocument)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:300
msgid ""
"Qt has several `layouts <https://doc.qt.io/qt-5/qlayout.html>`_, but the "
"`QHBoxLayout and the QVBoxLayout <https://doc.qt.io/qt-5/qboxlayout.html>`_ "
"are the easiest to use, they just arrange widgets horizontally or vertically."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:302
msgid "Restart Krita and the button should now be laid out nicely."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:305
msgid "PyQt Signals and Slots"
msgstr "PyQt signaler och slots"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:307
msgid ""
"We've already been using PyQt signals and slots already, but there are times "
"where you want to create your own signals and slots. `As pyQt's "
"documentation is pretty difficult to understand <https://www."
"riverbankcomputing.com/static/Docs/PyQt4/new_style_signals_slots.html>`_, "
"and the way how signals and slots are created is very different from C++ Qt, "
"we're explaining it here:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:310
msgid ""
"All python functions you make in PyQt can be understood as slots, meaning "
"that they can be connected to signals like Action.triggered or QPushButton."
"clicked. However, QCheckBox has a signal for toggled, which sends a boolean. "
"How do we get our function to accept that boolean?"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:312
msgid "First, make sure you have the right import for making custom slots:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:314
msgid "``from PyQt5.QtCore import pyqtSlot``"
msgstr "``from PyQt5.QtCore import pyqtSlot``"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:316
msgid ""
"(If there's from ``PyQt5.QtCore import *`` already in the list of imports, "
"then you won't have to do this, of course.)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:318
msgid "Then, you need to add a PyQt slot definition before your function:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:329
msgid ""
"@pyqtSlot(bool)\n"
"def myFunction(self, enabled):\n"
"    enabledString = \"disabled\"\n"
"    if (enabled == True):\n"
"        enabledString = \"enabled\"\n"
"    print(\"The checkbox is\"+enabledString)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:330
msgid ""
"Then, when you have created your checkbox, you can do something like "
"myCheckbox.toggled.connect(self.myFunction)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:332
msgid "Similarly, to make your own PyQt signals, you do the following:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:343
msgid ""
"# signal name is added to the member variables of the class\n"
"signal_name = pyqtSignal(bool, name='signalName')\n"
"\n"
"def emitMySignal(self):\n"
"    # And this is how you trigger the signal to be emitted.\n"
"    self.signal_name.emit(True)"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:344
msgid "And use the right import:"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:346
msgid "``from PyQt5.QtCore import pyqtSignal``"
msgstr "``from PyQt5.QtCore import pyqtSignal``"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:348
msgid ""
"To emit or create slots for objects that aren't standard python objects, you "
"only have to put their names between quotation marks."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:351
msgid "A note on unit tests"
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:353
msgid ""
"If you want to write unit tests for your plugin, have a look at the `mock "
"krita module <https://github.com/rbreu/krita-python-mock>`_."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:357
msgid "Conclusion"
msgstr "Avslutning"

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:359
msgid ""
"Okay, so that covers all the Krita specific details for creating python "
"plugins. It doesn't handle how to parse the pixel data, or best practices "
"with documents, but if you have a little bit of experience with python you "
"should be able to start creating your own plugins."
msgstr ""

#: ../../user_manual/python_scripting/krita_python_plugin_howto.rst:361
msgid ""
"As always, read the code carefully and read the API docs for python, Krita "
"and Qt carefully to see what is possible, and you'll get pretty far."
msgstr ""

#~ msgid "No clue"
#~ msgstr "Ingen aning"

#~ msgid "No Clue."
#~ msgstr "Ingen aning."
