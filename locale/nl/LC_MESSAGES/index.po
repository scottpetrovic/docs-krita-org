# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-05 17:09+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_userManual.jpg"
msgstr ".. image:: images/intro_page/Hero_userManual.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_tutorials.jpg"
msgstr ".. image:: images/intro_page/Hero_tutorials.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_getting_started.jpg"
msgstr ".. image:: images/intro_page/Hero_getting_started.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_reference.jpg"
msgstr ".. image:: images/intro_page/Hero_reference.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_general.jpg"
msgstr ".. image:: images/intro_page/Hero_general.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_faq.jpg"
msgstr ".. image:: images/intro_page/Hero_faq.jpg"

#: ../../index.rst:0
msgid ".. image:: images/intro_page/Hero_resources.jpg"
msgstr ".. image:: images/intro_page/Hero_resources.jpg"

#: ../../index.rst:5
msgid "Welcome to the Krita |version| Manual!"
msgstr "Welkom bij de Krita |version| handleiding!"

#: ../../index.rst:7
msgid "Welcome to Krita's documentation page."
msgstr "Welkom bij de documentatiepagina van Krita."

#: ../../index.rst:9
msgid ""
"Krita is a sketching and painting program designed for digital artists. Our "
"vision for Development of Krita is —"
msgstr ""
"Krita is een programma voor schetsen, tekenen en schilderen, ontworpen voor "
"digitale kunstenaars. Onze visie op ontwikkeling van Krita is —"

#: ../../index.rst:11
msgid ""
"Krita is a free and open source cross-platform application that offers an "
"end-to-end solution for creating digital art files from scratch. Krita is "
"optimized for frequent, prolonged and focused use. Explicitly supported "
"fields of painting are illustrations, concept art, matte painting, textures, "
"comics and animations. Developed together with users, Krita is an "
"application that supports their actual needs and workflow. Krita supports "
"open standards and interoperates with other applications."
msgstr ""
"Krita is een vrij en open-source cross-platform toepassing die een end-to-"
"end oplossing biedt voor het maken van bestanden met digitale kunst vanuit "
"het niets. Krita is geoptimaliseerd voor frequent, voortdurend en gefocust "
"gebruik. Expliciet ondersteunde velden voor schilderen zijn illustraties, "
"conceptuele kunst, mat schilderen, texturen, strips en animaties. Samen met "
"gebruikers ontwikkelt, is Krita een toepassing die hun actuele behoeften en "
"workflow ondersteunt. Krita ondersteunt open standaarden en werkt samen met "
"andere toepassingen."

#: ../../index.rst:19
msgid ""
"Krita's tools are developed keeping the above vision in mind. Although it "
"has features that overlap with other raster editors its intended purpose is "
"to provide robust tool for digital painting and creating artworks from "
"scratch. As you learn about Krita, keep in mind that it is not intended as a "
"replacement for Photoshop. This means that the other programs may have more "
"features than Krita for image manipulation tasks, such as stitching together "
"photos, while Krita's tools are most relevant to digital painting, concept "
"art, illustration, and texturing. This fact accounts for a great deal of "
"Krita's design."
msgstr ""
"Hulpmiddelen van Krita worden ontwikkeld met de bovenstaande visie in "
"gedachten. Hoewel het functies heeft die overlappen met andere "
"rasterbewerkers is zijn beoogde doel om een robuust hulpmiddel voor digitale "
"schilderen en kunstwerken maken vanuit het niets te maken. Bij te leren "
"werken met Krita, dient u te bedenken dat het niet bedoeld is als een "
"vervanging voor Photoshop. Dit betekent dat de andere programma's meer "
"functies kunnen hebben dan Krita voor taken met manipulatie van "
"afbeeldingen, zoals foto's aan elkaar plakken, terwijl de hulpmiddelen van "
"Krita's het meest relevant zijn voor digitaal schilderen, conceptuele kunst, "
"illustraties en textuur aanbrengen. Deze feiten zijn voor een groot deel "
"verantwoordelijk voor het ontwerp van Krita."

#: ../../index.rst:28
msgid ""
"You can download this manual as an epub `here <https://docs.krita.org/en/"
"epub/KritaManual.epub>`_."
msgstr ""
"U kunt deze handleiding als een epub `hier <https://docs.krita.org/en/epub/"
"KritaManual.epub>`_ downloaden."

#: ../../index.rst:33
msgid ":ref:`user_manual`"
msgstr ":ref:`user_manual`"

#: ../../index.rst:33
msgid ":ref:`tutorials`"
msgstr ":ref:`tutorials`"

#: ../../index.rst:35
msgid ""
"Discover Krita’s features through an online manual. Guides to help you "
"transition from other applications."
msgstr ""
"Ontdek de functies van Krita via een online handleiding. Is een gids om u te "
"helpen bij de overgang vanaf andere toepassingen."

#: ../../index.rst:35
msgid ""
"Learn through developer and user generated tutorials to see Krita in action."
msgstr ""
"Leer te werken via door ontwerper en gebruiker gegenereerde inleidingen om "
"Krita in actie te zien."

#: ../../index.rst:41
msgid ":ref:`getting_started`"
msgstr ":ref:`getting_started`"

#: ../../index.rst:41
msgid ":ref:`reference_manual`"
msgstr ":ref:`reference_manual`"

#: ../../index.rst:43
msgid "New to Krita and don't know where to start?"
msgstr "Nieuw bij Krita en niet weten waar te beginnen?"

#: ../../index.rst:43
msgid "A quick run-down of all of the tools that are available"
msgstr "Een snelle opsomming van alle beschikbare hulpmiddelen"

#: ../../index.rst:48
msgid ":ref:`general_concepts`"
msgstr ":ref:`general_concepts`"

#: ../../index.rst:48
msgid ":ref:`faq`"
msgstr ":ref:`faq`"

#: ../../index.rst:50
msgid ""
"Learn about general art and technology concepts that are not specific to "
"Krita."
msgstr ""
"Leer iets over concepten van algemene aard en technologie die niet specifiek "
"zijn voor Krita."

#: ../../index.rst:50
msgid ""
"Find answers to the most common questions about Krita and what it offers."
msgstr ""
"Vind antwoorden op de meest algemene vragen over Krita en wat het biedt."

#: ../../index.rst:55
msgid ":ref:`resources_page`"
msgstr ":ref:`resources_page`"

#: ../../index.rst:55
msgid ":ref:`genindex`"
msgstr ":ref:`genindex`"

#: ../../index.rst:57
msgid ""
"Textures, brush packs, and python plugins to help add variety to your "
"artwork."
msgstr ""
"Texturen, pakketten met penselen en plug-ins in Python om u te helpen aan uw "
"kunstwerken variatie toe te voegen."

#: ../../index.rst:57
msgid "An index of the manual for searching terms by browsing."
msgstr "Een index van de handleiding voor zoektermen via bladeren."
