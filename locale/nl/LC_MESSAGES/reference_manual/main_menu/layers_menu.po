# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-06 12:08+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Layerstyle (2.9.5+)"
msgstr "Lagenstijl (2.9.5+)"

#: ../../reference_manual/main_menu/layers_menu.rst:1
msgid "The layers menu in Krita."
msgstr "Het menu Lagen in Krita."

#: ../../reference_manual/main_menu/layers_menu.rst:11
#: ../../reference_manual/main_menu/layers_menu.rst:16
msgid "Layers"
msgstr "Lagen"

#: ../../reference_manual/main_menu/layers_menu.rst:11
#: ../../reference_manual/main_menu/layers_menu.rst:75
msgid "Convert"
msgstr "Converteren"

#: ../../reference_manual/main_menu/layers_menu.rst:11
#: ../../reference_manual/main_menu/layers_menu.rst:115
msgid "Transform"
msgstr "Transformeren"

#: ../../reference_manual/main_menu/layers_menu.rst:11
#: ../../reference_manual/main_menu/layers_menu.rst:134
msgid "Histogram"
msgstr "Histogram"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Cut Layer"
msgstr "Laag knippen"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Copy Layer"
msgstr "Laag kopiëren"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Paste Layer"
msgstr "Laag plakken"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Import"
msgstr "Importeren"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Export"
msgstr "Exporteren"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Metadata"
msgstr "Metagegevens"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Flatten"
msgstr "Plat maken"

#: ../../reference_manual/main_menu/layers_menu.rst:11
msgid "Layer Style"
msgstr "Laagstijlen"

#: ../../reference_manual/main_menu/layers_menu.rst:18
msgid ""
"These are the topmenu options are related to Layer Management, check out :"
"ref:`that page <layers_and_masks>` first, if you haven't."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:20
msgid "Cut Layer (3.0+)"
msgstr "Laag knippen (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:21
msgid "Cuts the whole layer rather than just the pixels."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:22
msgid "Copy Layer (3.0+)"
msgstr "Laag kopiëren (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:23
msgid "Copy the whole layer rather than just the pixels."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:24
msgid "Paste Layer (3.0+)"
msgstr "Laag plakken (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:25
msgid "Pastes the whole layer if any of the top two actions have been taken."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:27
#: ../../reference_manual/main_menu/layers_menu.rst:41
#: ../../reference_manual/main_menu/layers_menu.rst:60
#: ../../reference_manual/main_menu/layers_menu.rst:78
#: ../../reference_manual/main_menu/layers_menu.rst:92
#: ../../reference_manual/main_menu/layers_menu.rst:102
#: ../../reference_manual/main_menu/layers_menu.rst:118
msgid "Organizes the following actions:"
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:29
msgid "Paint Layer"
msgstr "Tekenlaag"

#: ../../reference_manual/main_menu/layers_menu.rst:30
msgid "Add a new paint layer"
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:31
msgid "New layer from visible (3.0.2+)"
msgstr "Nieuwe laag uit zichtbaar (3.0.2+)"

#: ../../reference_manual/main_menu/layers_menu.rst:32
msgid "Add a new layer with the visible pixels."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:33
msgid "Duplicate Layer or Mask"
msgstr "Laag of masker dupliceren"

#: ../../reference_manual/main_menu/layers_menu.rst:34
msgid "Duplicates the layer."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:35
msgid "Cut Selection to New Layer"
msgstr "Selectie naar nieuwe laag knippen"

#: ../../reference_manual/main_menu/layers_menu.rst:36
msgid "Single action for cut+paste"
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:38
msgid "New"
msgstr "Nieuw"

#: ../../reference_manual/main_menu/layers_menu.rst:38
msgid "Copy Selection to New Layer"
msgstr "Selectie naar nieuwe laag kopiëren"

#: ../../reference_manual/main_menu/layers_menu.rst:38
msgid "Single action for copy+paste"
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:43
msgid "Save Layer or Mask"
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:44
msgid "Saves the Layer or Mask as a separate image."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:45
msgid "Save Vector Layer as SVG"
msgstr "Vectorlaag opslaan als SVG"

#: ../../reference_manual/main_menu/layers_menu.rst:46
msgid "Save the currently selected vector layer as an SVG."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:47
msgid "Save Group Layers"
msgstr "Gegroepeerde lagen opslaan"

#: ../../reference_manual/main_menu/layers_menu.rst:48
msgid "Saves the top-level group layers as single-layer images."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:49
msgid "Import Layer"
msgstr "Laag importeren"

#: ../../reference_manual/main_menu/layers_menu.rst:50
msgid "Import an image as a layer into the current file."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:52
msgid ""
"Import an image as a specific layer type. The following layer types are "
"supported:"
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:54
msgid "Paint layer"
msgstr "Tekenlaag"

#: ../../reference_manual/main_menu/layers_menu.rst:55
#: ../../reference_manual/main_menu/layers_menu.rst:66
msgid "Transparency Mask"
msgstr "Transparantiemasker"

#: ../../reference_manual/main_menu/layers_menu.rst:56
#: ../../reference_manual/main_menu/layers_menu.rst:68
msgid "Filter Mask"
msgstr "Filtermasker"

#: ../../reference_manual/main_menu/layers_menu.rst:57
msgid "Import/Export"
msgstr "Importeren/Exporteren"

#: ../../reference_manual/main_menu/layers_menu.rst:57
msgid "Import as..."
msgstr "Importeren als..."

#: ../../reference_manual/main_menu/layers_menu.rst:57
#: ../../reference_manual/main_menu/layers_menu.rst:70
msgid "Selection Mask"
msgstr "Selectiemasker"

#: ../../reference_manual/main_menu/layers_menu.rst:62
msgid "Convert a layer to..."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:64
msgid "Convert to Paint Layer"
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:65
msgid "Convert a mask or vector layer to a paint layer."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:67
msgid ""
"Convert a layer to a transparency mask. The image will be converted to "
"grayscale first, and these grayscale values are used to drive the "
"transparency."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:69
msgid ""
"Convert a layer to a filter mask. The image will be converted to grayscale "
"first, and these grayscale values are used to drive the filter effect area."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:71
msgid ""
"Convert a layer to a selection mask. The image will be converted to "
"grayscale first, and these grayscale values are used to drive the selected "
"area."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:72
msgid "Convert Group to Animated Layer"
msgstr "Groep naar een geanimeerde laag converteren"

#: ../../reference_manual/main_menu/layers_menu.rst:73
msgid ""
"This takes the images in the group layer and makes them into frames of an "
"animated layer."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:75
msgid "Convert Layer Color Space"
msgstr "Kleurruimte van laag converteren"

#: ../../reference_manual/main_menu/layers_menu.rst:75
msgid "This only converts the color space of the layer, not the image."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:80
msgid "All layers"
msgstr "Alle lagen"

#: ../../reference_manual/main_menu/layers_menu.rst:81
msgid "Select all layers."
msgstr "Alle lagen selecteren."

#: ../../reference_manual/main_menu/layers_menu.rst:82
msgid "Visible Layers"
msgstr "Zichtbare lagen"

#: ../../reference_manual/main_menu/layers_menu.rst:83
msgid "Select all visible layers."
msgstr "Alle zichtbare lagen selecteren."

#: ../../reference_manual/main_menu/layers_menu.rst:84
msgid "Invisible Layers"
msgstr "Onzichtbare lagen"

#: ../../reference_manual/main_menu/layers_menu.rst:85
msgid "Select all invisible layers, useful for cleaning up a sketch."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:86
msgid "Locked Layers"
msgstr "Vergrendelde lagen"

#: ../../reference_manual/main_menu/layers_menu.rst:87
msgid "Select all locked layers."
msgstr "Alle vergrendelde lagen selecteren."

#: ../../reference_manual/main_menu/layers_menu.rst:89
msgid "Select (3.0+):"
msgstr "Selecteren (3.0+):"

#: ../../reference_manual/main_menu/layers_menu.rst:89
msgid "Unlocked Layers"
msgstr "Ontgrendeld lagen"

#: ../../reference_manual/main_menu/layers_menu.rst:89
msgid "Select all unlocked layers."
msgstr "Alle ontgrendelde lagen selecteren."

#: ../../reference_manual/main_menu/layers_menu.rst:94
msgid "Quick Group (3.0+)"
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:95
msgid "Adds all selected layers to a group."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:96
msgid "Quick Clipping Group (3.0+)"
msgstr "Snelle afsnijgroep (3.0+)"

#: ../../reference_manual/main_menu/layers_menu.rst:97
msgid ""
"Adds all selected layers to a group and adds a alpha-inherited layer above "
"it."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:99
msgid "Group"
msgstr "Groep"

#: ../../reference_manual/main_menu/layers_menu.rst:99
msgid "Quick Ungroup"
msgstr "Snel groepering opheffen"

#: ../../reference_manual/main_menu/layers_menu.rst:99
msgid "Ungroups the activated layer."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:104
msgid "Mirror Layer Horizontally"
msgstr "Laag horizontaal spiegelen"

#: ../../reference_manual/main_menu/layers_menu.rst:105
msgid "Mirror the layer horizontally using the image center."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:106
msgid "Mirror Layer Vertically"
msgstr "Laag verticaal spiegelen"

#: ../../reference_manual/main_menu/layers_menu.rst:107
msgid "Mirror the layer vertically using the image center."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:108
msgid "Rotate"
msgstr "Draaien"

#: ../../reference_manual/main_menu/layers_menu.rst:109
msgid "Rotate the layer around the image center."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:110
msgid "Scale Layer"
msgstr "Laag schalen"

#: ../../reference_manual/main_menu/layers_menu.rst:111
msgid ""
"Scale the layer by the given amounts using the given interpolation filter."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:112
msgid "Shear Layer"
msgstr "Laag schuintrekken"

#: ../../reference_manual/main_menu/layers_menu.rst:113
msgid "Shear the layer pixels by the given X and Y angles."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:115
msgid "Offset Layer"
msgstr "Offset van laag"

#: ../../reference_manual/main_menu/layers_menu.rst:115
msgid "Offset the layer pixels by a given amount."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:120
msgid "Split Alpha"
msgstr "Alfa splitsen"

#: ../../reference_manual/main_menu/layers_menu.rst:121
msgid ""
"Split the image transparency into a mask. This is useful when you wish to "
"edit the transparency separately."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:122
msgid "Split Layer"
msgstr "Laag splitsen"

#: ../../reference_manual/main_menu/layers_menu.rst:123
msgid ":ref:`Split the layer <split_layer>` into given color fields."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:125
msgid "Split..."
msgstr "Splitsen..."

#: ../../reference_manual/main_menu/layers_menu.rst:125
msgid "Clones Array"
msgstr "Kloont een array"

#: ../../reference_manual/main_menu/layers_menu.rst:125
msgid ""
"A complex bit of functionality to generate clone-layers for quick sprite "
"making. See :ref:`clones_array` for more details."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:127
msgid "Edit Metadata"
msgstr "Metagegevens bewerken"

#: ../../reference_manual/main_menu/layers_menu.rst:128
msgid "Each layer can have its own metadata."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:130
msgid "Shows a histogram."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:134
msgid "Removed. Use the :ref:`histogram_docker` instead."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:136
msgid "Merge With Layer Below"
msgstr "Met onderliggende laag samenvoegen"

#: ../../reference_manual/main_menu/layers_menu.rst:137
msgid "Merge a layer down."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:138
msgid "Flatten Layer"
msgstr "Laag waar andere lagen in terecht komen"

#: ../../reference_manual/main_menu/layers_menu.rst:139
msgid "Flatten a Group Layer or flatten the masks into any other layer."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:140
msgid "Rasterize Layer"
msgstr "Laag voor rasteriseren"

#: ../../reference_manual/main_menu/layers_menu.rst:141
msgid "For making vectors into raster layers."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:142
msgid "Flatten Image"
msgstr "Afbeelding plat maken"

#: ../../reference_manual/main_menu/layers_menu.rst:143
msgid "Flatten all layers into one."
msgstr ""

#: ../../reference_manual/main_menu/layers_menu.rst:145
msgid "Set the PS-style layerstyle"
msgstr ""
