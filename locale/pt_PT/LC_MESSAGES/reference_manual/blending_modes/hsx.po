# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-18 12:00+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: BlendingmodesColorHSILightblueandOrange image HSI\n"
"X-POFile-SpellExtra: BlendingmodesColorHSLSampleimagewithdots HSL\n"
"X-POFile-SpellExtra: blendingmodes images HSX HSY\n"
"X-POFile-SpellExtra: BlendingmodesColorHSISampleimagewithdots\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: BlendingmodesDecreaseSaturationHSVSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesIncreaseLuminositySampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesLightnessSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesLuminositySampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesIntensitySampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesDecreaseIntensityLightblueandOrange\n"
"X-POFile-SpellExtra: BlendingmodesHueSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesIncreaseSaturationHSLSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesIncreaseLightnessSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesIncreaseSaturationSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesHueHSLSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesHueHSVSampleimagewithdots HSB\n"
"X-POFile-SpellExtra: BlendingmodesColorHSVSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesSaturationSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesDecreaseSaturationHSLSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesDecreaseIntensitySampleimagewithdots rgb\n"
"X-POFile-SpellExtra: BlendingmodesSaturationHSVSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesDecreaseSaturationHSILightblueandOrange\n"
"X-POFile-SpellExtra: Luma\n"
"X-POFile-SpellExtra: BlendingmodesIncreaseSaturationHSISampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesValueSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesDecreaseSaturationHSISampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesColorSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesSaturationHSLSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesDecreaseValueSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesHueHSISampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesIncreaseSaturationHSVSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesDecreaseLuminositySampleimagewithdots\n"
"X-POFile-SpellExtra: aclaramento\n"
"X-POFile-SpellExtra: BlendingmodesSaturationHSISampleimagewithdots Krita\n"
"X-POFile-SpellExtra: BlendingmodesIncreaseIntensitySampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesIncreaseValueSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesDecreaseLightnessSampleimagewithdots\n"
"X-POFile-SpellExtra: BlendingmodesDecreaseSaturationSampleimagewithdots\n"

#: ../../reference_manual/blending_modes/hsx.rst:1
msgid ""
"Page about the HSX blending modes in Krita, amongst which Hue, Color, "
"Luminosity and Saturation."
msgstr ""
"Página sobre os modos de mistura HSX no Krita, entre os quais o da Matiz, "
"Cor, Luminosidade e Saturação."

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:202
msgid "Intensity"
msgstr "Intensidade"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:214
msgid "Value"
msgstr "Valor"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:226
msgid "Lightness"
msgstr "Ĩluminação"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:238
msgid "Luminosity"
msgstr "Luminosidade"

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Hue"
msgstr "Tom"

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Saturation"
msgstr "Saturação"

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Luma"
msgstr "Luma"

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Brightness"
msgstr "Brilho"

#: ../../reference_manual/blending_modes/hsx.rst:15
msgid "HSX"
msgstr "HSX"

#: ../../reference_manual/blending_modes/hsx.rst:17
msgid ""
"Krita has four different HSX coordinate systems. The difference between them "
"is how they handle tone."
msgstr ""
"O Krita tem quatro sistemas de coordenadas HSX diferentes. A diferença entre "
"eles é como lidam com a tonalidade."

#: ../../reference_manual/blending_modes/hsx.rst:20
msgid "HSI"
msgstr "HSI"

#: ../../reference_manual/blending_modes/hsx.rst:22
msgid ""
"HSI is a color coordinate system, using Hue, Saturation and Intensity to "
"categorize a color. Hue is roughly the wavelength, whether the color is red, "
"yellow, green, cyan, blue or purple. It is measure in 360°, with 0 being "
"red. Saturation is the measurement of how close a color is to gray. "
"Intensity, in this case is the tone of the color. What makes intensity "
"special is that it recognizes yellow (rgb:1,1,0) having a higher combined "
"rgb value than blue (rgb:0,0,1). This is a non-linear tone dimension, which "
"means it's gamma-corrected."
msgstr ""
"O HSI é um sistema de coordenadas de cores, usando a Matiz, a Saturação e a "
"Intensidade para classificar uma dada cor. A Matiz corresponde de certa "
"forma ao comprimento de onda, que define se a cor é vermelha, amarela, "
"verde, cíano, azul ou roxa. É medida em 360°, sendo que o 0 é o vermelho. A "
"Saturação é a medida da proximidade de uma cor ao cinzento. A Intensidade, "
"neste caso, é o tom da cor. O que torna a intensidade especial é que "
"reconhece o amarelo (rgb:1, 1, 0) como tendo um valor RGB combinado mais "
"elevado que o azul (rgb:0, 0, 1). Esta é uma dimensão não-linear da "
"tonalidade, o que significa que tem correcção do gama."

#: ../../reference_manual/blending_modes/hsx.rst:28
msgid "HSL"
msgstr "HSL"

#: ../../reference_manual/blending_modes/hsx.rst:30
msgid ""
"HSL is also a color coordinate system. It describes colors in Hue, "
"Saturation and Lightness. Lightness specifically puts both yellow "
"(rgb:1,1,0), blue (rgb:0,0,1) and middle gray (rgb:0.5,0.5,0.5) at the same "
"lightness (0.5)."
msgstr ""
"O HSL também é um sistema de coordenadas de cores. Ele descreve as cores em "
"termos de Matiz, Saturação e Iluminação. A iluminação define especificamente "
"o amarelo (rgb:1, 1, 0), azul (rgb:0, 0, 1) e cinzento médio (rgb:0,5, 0,5, "
"0,5) com a mesma iluminação (0,5)."

#: ../../reference_manual/blending_modes/hsx.rst:34
msgid "HSV"
msgstr "HSV"

#: ../../reference_manual/blending_modes/hsx.rst:36
msgid ""
"HSV, occasionally called HSB, is a color coordinate system. It measures "
"colors in Hue, Saturation, and Value (also called Brightness). Value or "
"Brightness specifically refers to strength at which the pixel-lights on your "
"monitor have to shine. It sets Yellow (rgb:1,1,0), Blue (rgb:0,0,1) and "
"White (rgb:1,1,0) at the same Value (100%)"
msgstr ""
"O HSV, ocasionalmente referido como HSB, é um sistema de coordenadas de "
"cores. Ele mede a cor em termos de Matiz, Saturação e Valor (também chamado "
"de Brilho). O Valor ou o Brilho refere-se em específico à potência com que "
"as luzes dos pixels do seu monitor têm de brilhar. Isso define o Amarelo "
"(rgb:1, 1, 0), o Azul (rgb:0, 0, 1) e o Branco (rgb:1, 1, 0) como tendo o "
"mesmo Valor (100%)"

#: ../../reference_manual/blending_modes/hsx.rst:40
msgid "HSY"
msgstr "HSY"

#: ../../reference_manual/blending_modes/hsx.rst:42
msgid ""
"HSY is a color coordinate system. It categorizes colors in Hue, Saturation "
"and Luminosity. Well, not really, it uses Luma instead of true luminosity, "
"the difference being that Luminosity is linear while Luma is gamma-corrected "
"and just weights the rgb components. Luma is based on scientific studies of "
"how much light a color reflects in real-life. While like intensity it "
"acknowledges that yellow (rgb:1,1,0) is lighter than blue (rgb:0,0,1), it "
"also acknowledges that yellow (rgb:1,1,0) is lighter than cyan (rgb:0,1,1), "
"based on these studies."
msgstr ""
"O HSY é um sistema de coordenadas de cores, classificando as cores em termos "
"de  Matiz, Saturação e Luminosidade. Bem, na verdade, usa o Luma em vez da "
"luminosidade, sendo diferente na medida em que a Luminosidade é linear, "
"enquanto a Luma tem a correcção do gama e apenas define os pesos dos "
"componentes RGB. O Luma baseia-se nos estudos científicos em quanta luz uma "
"dada cor reflecte na vida-real. Nessa medida, como a intensidade, confirma "
"que o amarelo (rgb:1, 1, 0) é mais claro que o azul (rgb:0, 0, 1), "
"confirmando também que o amarelo (rgb:1, 1, 0) é mais claro que o cíano "
"(rgb:0, 1, 1), baseado nesses estudos."

#: ../../reference_manual/blending_modes/hsx.rst:46
msgid "HSX Blending Modes"
msgstr "Modos de Mistura HSX"

#: ../../reference_manual/blending_modes/hsx.rst:55
msgid "Color, HSV, HSI, HSL, HSY"
msgstr "Cor, HSV, HSI, HSL, HSY"

#: ../../reference_manual/blending_modes/hsx.rst:57
msgid ""
"This takes the Luminosity/Value/Intensity/Lightness of the colors on the "
"lower layer, and combines them with the Saturation and Hue of the upper "
"pixels. We refer to Color HSY as 'Color' in line with other applications."
msgstr ""
"Isto usa a Luminosidade/Valor/Intensidade/Iluminação das cores na camada "
"inferior e combina-as com a Saturação e Matiz dos pixels superiores. Usa-se "
"como referência a Cor do HSY como 'Cor', em conformidade com as outras "
"aplicações."

#: ../../reference_manual/blending_modes/hsx.rst:62
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Color_HSI_Gray_0.4_and_Gray_0.5.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSI_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/hsx.rst:62
#: ../../reference_manual/blending_modes/hsx.rst:67
#: ../../reference_manual/blending_modes/hsx.rst:72
msgid "Left: **Normal**. Right: **Color HSI**."
msgstr "Esquerda: **Normal**. Direita: **HSI Cor**."

#: ../../reference_manual/blending_modes/hsx.rst:67
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Color_HSI_Light_blue_and_Orange.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSI_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/hsx.rst:72
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Color_HSI_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:78
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Color_HSL_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:78
msgid "Left: **Normal**. Right: **Color HSL**."
msgstr "Esquerda: **Normal**. Direita: **HSL Cor**."

#: ../../reference_manual/blending_modes/hsx.rst:84
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Color_HSV_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:84
msgid "Left: **Normal**. Right: **Color HSV**."
msgstr "Esquerda: **Normal**. Direita: **Cor HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:90
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Color_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/Blending_modes_Color_Sample_image_with_dots."
"png"

#: ../../reference_manual/blending_modes/hsx.rst:90
msgid "Left: **Normal**. Right: **Color**."
msgstr "Esquerda: **Normal**. Direita: **Cor**."

#: ../../reference_manual/blending_modes/hsx.rst:99
msgid "Hue HSV, HSI, HSL, HSY"
msgstr "Matiz HSV, HSI, HSL, HSY"

#: ../../reference_manual/blending_modes/hsx.rst:101
msgid ""
"Takes the saturation and tone of the lower layer and combines them with the "
"hue of the upper-layer. Tone in this case being either Value, Lightness, "
"Intensity or Luminosity."
msgstr ""
"Isto usa a saturação e a tonalidade da camada inferior e combina-as com a "
"matiz da camada superior. A tonalidade será neste caso a Luminosidade, "
"Valor, Intensidade ou Iluminação."

#: ../../reference_manual/blending_modes/hsx.rst:107
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Hue_HSI_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Hue_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:107
msgid "Left: **Normal**. Right: **Hue HSI**."
msgstr "Esquerda: **Normal**. Direita: **Matiz HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:113
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Hue_HSL_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Hue_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:113
msgid "Left: **Normal**. Right: **Hue HSL**."
msgstr "Esquerda: **Normal**. Direita: **Matiz HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:119
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Hue_HSV_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Hue_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:119
msgid "Left: **Normal**. Right: **Hue HSV**."
msgstr "Esquerda: **Normal**. Direita: **Matiz HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:125
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Hue_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/Blending_modes_Hue_Sample_image_with_dots."
"png"

#: ../../reference_manual/blending_modes/hsx.rst:125
msgid "Left: **Normal**. Right: **Hue**."
msgstr "Esquerda: **Normal**. Direita: **Matiz**."

#: ../../reference_manual/blending_modes/hsx.rst:134
msgid "Increase Value, Lightness, Intensity or Luminosity."
msgstr "Aumentar o Valor, Luminosidade, Intensidade ou Iluminação."

#: ../../reference_manual/blending_modes/hsx.rst:136
msgid ""
"Similar to lighten, but specific to tone. Checks whether the upper layer's "
"pixel has a higher tone than the lower layer's pixel. If so, the tone is "
"increased, if not, the lower layer's tone is maintained."
msgstr ""
"Semelhante ao aclaramento, mas é específico do tom. Verifica se o pixel da "
"camada superior tem um tom mais claro que a camada inferior. Se for o caso, "
"a intensidade é aumentada; caso contrário, o tom da camada inferior é "
"mantido."

#: ../../reference_manual/blending_modes/hsx.rst:142
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Intensity_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Intensity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Intensity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:142
msgid "Left: **Normal**. Right: **Increase Intensity**."
msgstr "Esquerda: **Normal**. Direita: **Aumento da Intensidade**."

#: ../../reference_manual/blending_modes/hsx.rst:148
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Lightness_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Lightness_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Lightness_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:148
msgid "Left: **Normal**. Right: **Increase Lightness**."
msgstr "Esquerda: **Normal**. Direita: **Aumento da Iluminação**."

#: ../../reference_manual/blending_modes/hsx.rst:154
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Value_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Value_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Value_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:154
msgid "Left: **Normal**. Right: **Increase Value**."
msgstr "Esquerda: **Normal**. Direita: **Aumento do Valor**."

#: ../../reference_manual/blending_modes/hsx.rst:160
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Luminosity_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Luminosity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Luminosity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:160
msgid "Left: **Normal**. Right: **Increase Luminosity**."
msgstr "Esquerda: **Normal**. Direita: **Aumento da Luminosidade**."

#: ../../reference_manual/blending_modes/hsx.rst:170
msgid "Increase Saturation HSI, HSV, HSL, HSY"
msgstr "Aumento da Saturação HSI, HSV, HSL, HSY"

#: ../../reference_manual/blending_modes/hsx.rst:172
msgid ""
"Similar to lighten, but specific to Saturation. Checks whether the upper "
"layer's pixel has a higher Saturation than the lower layer's pixel. If so, "
"the Saturation is increased, if not, the lower layer's Saturation is "
"maintained."
msgstr ""
"Semelhante ao aclaramento, mas é específico da saturação. Verifica se o "
"pixel da camada superior tem uma maior saturação que a camada inferior. Se "
"for o caso, a saturação é aumentada; caso contrário, a saturação da camada "
"inferior é mantida."

#: ../../reference_manual/blending_modes/hsx.rst:178
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Saturation_HSI_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:178
msgid "Left: **Normal**. Right: **Increase Saturation HSI**."
msgstr "Esquerda: **Normal**. Direita: **Aumento da Saturação HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:184
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Saturation_HSL_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:184
msgid "Left: **Normal**. Right: **Increase Saturation HSL**."
msgstr "Esquerda: **Normal**. Direita: **Aumento da Saturação HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:190
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Saturation_HSV_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:190
msgid "Left: **Normal**. Right: **Increase Saturation HSV**."
msgstr "Esquerda: **Normal**. Direita: **Aumento da Saturação HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:196
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Increase_Saturation_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:196
msgid "Left: **Normal**. Right: **Increase Saturation**."
msgstr "Esquerda: **Normal**. Direita: **Aumento da Saturação**."

#: ../../reference_manual/blending_modes/hsx.rst:204
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"intensity of the upper layer."
msgstr ""
"Isto usa a Matiz e Saturação da camada inferior e e combina-as com a "
"intensidade da camada superior."

#: ../../reference_manual/blending_modes/hsx.rst:209
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Intensity_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Intensity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Intensity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:209
msgid "Left: **Normal**. Right: **Intensity**."
msgstr "Esquerda: **Normal**. Direita: **Intensidade**."

#: ../../reference_manual/blending_modes/hsx.rst:216
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"Value of the upper layer."
msgstr ""
"Isto usa o Valor e Saturação da camada inferior e e combina-as com o valor "
"da camada superior."

#: ../../reference_manual/blending_modes/hsx.rst:221
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Value_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Value_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/Blending_modes_Value_Sample_image_with_dots."
"png"

#: ../../reference_manual/blending_modes/hsx.rst:221
msgid "Left: **Normal**. Right: **Value**."
msgstr "Esquerda: **Normal**. Direita: **Valor**."

#: ../../reference_manual/blending_modes/hsx.rst:228
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"Lightness of the upper layer."
msgstr ""
"Isto usa a Matiz e Saturação da camada inferior e e combina-as com a "
"iluminação da camada superior."

#: ../../reference_manual/blending_modes/hsx.rst:233
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Lightness_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Lightness_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Lightness_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:233
msgid "Left: **Normal**. Right: **Lightness**."
msgstr "Esquerda: **Normal**. Direita: **Iluminação**."

#: ../../reference_manual/blending_modes/hsx.rst:240
msgid ""
"As explained above, actually Luma, but called this way as it's in line with "
"the terminology in other applications. Takes the Hue and Saturation of the "
"Lower layer and outputs them with the Luminosity of the upper layer. The "
"most preferred one of the four Tone blending modes, as this one gives fairly "
"intuitive results for the Tone of a hue"
msgstr ""
"Como foi explicado, é de facto o Luma, mas é chamado desta forma para estar "
"alinhado com a terminologia nas outras aplicações. Selecciona a Matiz e a "
"Saturação da camada inferior e combina-a com a luminosidade da camada "
"superior. Sendo o método preferido dos quatro modos de mistura de tom, já "
"que gera resultados relativamente intuitivos para o tom de uma cor"

#: ../../reference_manual/blending_modes/hsx.rst:247
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Luminosity_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Luminosity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Luminosity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:247
msgid "Left: **Normal**. Right: **Luminosity**."
msgstr "Esquerda: **Normal**. Direita: **Luminosidade**."

#: ../../reference_manual/blending_modes/hsx.rst:256
msgid "Saturation HSI, HSV, HSL, HSY"
msgstr "Saturação HSI, HSV, HSL, HSY"

#: ../../reference_manual/blending_modes/hsx.rst:258
msgid ""
"Takes the Intensity and Hue of the lower layer, and outputs them with the "
"HSI saturation of the upper layer."
msgstr ""
"Isto usa a Intensidade e a Matiz da camada inferior e e combina-as com a "
"saturação HSI da camada superior."

#: ../../reference_manual/blending_modes/hsx.rst:263
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Saturation_HSI_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:263
msgid "Left: **Normal**. Right: **Saturation HSI**."
msgstr "Esquerda: **Normal**. Direita: **Saturação HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:269
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Saturation_HSL_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:269
msgid "Left: **Normal**. Right: **Saturation HSL**."
msgstr "Esquerda: **Normal**. Direita: **Saturação HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:275
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Saturation_HSV_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:275
msgid "Left: **Normal**. Right: **Saturation HSV**."
msgstr "Esquerda: **Normal**. Direita: **Saturação HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:281
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Saturation_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:281
msgid "Left: **Normal**. Right: **Saturation**."
msgstr "Esquerda: **Normal**. Direita: **Saturação**."

#: ../../reference_manual/blending_modes/hsx.rst:289
msgid "Decrease Value, Lightness, Intensity or Luminosity"
msgstr "Diminuição do Valor, Iluminação, Intensidade ou Luminosidade"

#: ../../reference_manual/blending_modes/hsx.rst:291
msgid ""
"Similar to darken, but specific to tone. Checks whether the upper layer's "
"pixel has a lower tone than the lower layer's pixel. If so, the tone is "
"decreased, if not, the lower layer's tone is maintained."
msgstr ""
"Semelhante ao escurecimento, mas é específico do tom. Verifica se o pixel da "
"camada superior tem um tom mais escuro que a camada inferior. Se for o caso, "
"o tom é diminuído; caso contrário, o tom da camada inferior é mantido."

#: ../../reference_manual/blending_modes/hsx.rst:297
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Intensity_Gray_0.4_and_Gray_0.5.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Intensity_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/hsx.rst:297
#: ../../reference_manual/blending_modes/hsx.rst:302
#: ../../reference_manual/blending_modes/hsx.rst:307
msgid "Left: **Normal**. Right: **Decrease Intensity**."
msgstr "Esquerda: **Normal**. Direita: **Diminuição da Intensidade**."

#: ../../reference_manual/blending_modes/hsx.rst:302
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Intensity_Light_blue_and_Orange.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Intensity_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/hsx.rst:307
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Intensity_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Intensity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:313
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Lightness_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Lightness_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Lightness_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:313
msgid "Left: **Normal**. Right: **Decrease Lightness**."
msgstr "Esquerda: **Normal**. Direita: **Diminuição da Iluminação**."

#: ../../reference_manual/blending_modes/hsx.rst:319
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Value_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Value_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Value_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:319
msgid "Left: **Normal**. Right: **Decrease Value**."
msgstr "Esquerda: **Normal**. Direita: **Diminuição do Valor**."

#: ../../reference_manual/blending_modes/hsx.rst:325
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Luminosity_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Luminosity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Luminosity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:325
msgid "Left: **Normal**. Right: **Decrease Luminosity**."
msgstr "Esquerda: **Normal**. Direita: **Diminuição da Luminosidade**."

#: ../../reference_manual/blending_modes/hsx.rst:334
msgid "Decrease Saturation HSI, HSV, HSL, HSY"
msgstr "Diminuição da Saturação HSI, HSV, HSL, HSY"

#: ../../reference_manual/blending_modes/hsx.rst:336
msgid ""
"Similar to darken, but specific to Saturation. Checks whether the upper "
"layer's pixel has a lower Saturation than the lower layer's pixel. If so, "
"the Saturation is decreased, if not, the lower layer's Saturation is "
"maintained."
msgstr ""
"Semelhante ao escurecimento, mas é específico da saturação. Verifica se o "
"pixel da camada superior tem uma menor saturação que a camada inferior. Se "
"for o caso, a saturação é diminuída; caso contrário, a saturação da camada "
"inferior é mantida."

#: ../../reference_manual/blending_modes/hsx.rst:342
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Saturation_HSI_Gray_0.4_and_Gray_0.5.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSI_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/hsx.rst:342
#: ../../reference_manual/blending_modes/hsx.rst:347
#: ../../reference_manual/blending_modes/hsx.rst:352
msgid "Left: **Normal**. Right: **Decrease Saturation HSI**."
msgstr "Esquerda: **Normal**. Direita: **Diminuição da Saturação HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:347
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Saturation_HSI_Light_blue_and_Orange.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSI_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/hsx.rst:352
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Saturation_HSI_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:358
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Saturation_HSL_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:358
msgid "Left: **Normal**. Right: **Decrease Saturation HSL**."
msgstr "Esquerda: **Normal**. Direita: **Diminuição da Saturação HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:364
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Saturation_HSV_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:364
msgid "Left: **Normal**. Right: **Decrease Saturation HSV**."
msgstr "Esquerda: **Normal**. Direita: **Diminuição da Saturação HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:370
#, fuzzy
#| msgid ""
#| ".. image:: images/blending_modes/"
#| "Blending_modes_Decrease_Saturation_Sample_image_with_dots.png"
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:370
msgid "Left: **Normal**. Right: **Decrease Saturation**."
msgstr "Esquerda: **Normal**. Direita: **Diminuição da Saturação**."
