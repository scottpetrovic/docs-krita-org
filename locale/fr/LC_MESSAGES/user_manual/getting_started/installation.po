msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-09 03:41+0200\n"
"PO-Revision-Date: 2019-02-27 08:32+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../user_manual/getting_started/installation.rst:1
msgid "Detailed steps on how to install Krita"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:14
#: ../../user_manual/getting_started/installation.rst:18
#, fuzzy
msgid "Installation"
msgstr "Installation"

#: ../../user_manual/getting_started/installation.rst:21
#, fuzzy
msgid "Windows"
msgstr "Windows"

#: ../../user_manual/getting_started/installation.rst:22
msgid ""
"Windows users can download Krita from the website, the Windows Store, or "
"Steam."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:24
msgid ""
"The versions on the Store and Steam cost money, but are `functionally "
"identical <https://krita.org/en/item/krita-available-from-the-windows-store/"
">`_ to the (free) website version. Unlike the website version, however, both "
"paid versions get automatic updates when new versions of Krita comes out. "
"After deduction of the Store fee, the purchase cost supports Krita "
"development."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:31
msgid ""
"The latest version is always on our `website <https://krita.org/download/>`_."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:33
msgid ""
"The page will try to automatically recommend the correct architecture (64- "
"or 32-bit), but you can select \"All Download Versions\" to get more "
"choices. To determine your computer architecture manually, go to :"
"menuselection:`Settings --> About`. Your architecture will be listed as the :"
"guilabel:`System Type` in the :guilabel:`Device Specifications` section."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:35
msgid ""
"Krita by default downloads an **installer EXE**, but you can also download a "
"**portable zip-file** version instead. Unlike the installer version, this "
"portable version does not show previews in Windows Explorer automatically. "
"To get these previews with the portable version, also install Krita's "
"**Windows Shell Extension** extension (available on the download page)."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:36
msgid "Website:"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:37
msgid ""
"These files are also available from the `KDE download directory <http://"
"download.kde.org/stable/krita/>`_."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:38
#, fuzzy
msgid "Windows Store:"
msgstr "Windows"

#: ../../user_manual/getting_started/installation.rst:39
msgid ""
"For a small fee, you can download Krita `from the Windows Store <https://www."
"microsoft.com/store/productId/9N6X57ZGRW96>`_. This version requires Windows "
"10."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:41
msgid ""
"For a small fee, you can also download Krita `from Steam <https://store."
"steampowered.com/app/280680/Krita/>`_."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:42
msgid "Steam:"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:44
#, fuzzy
#| msgid ""
#| "Windows users can download the latest releases from our `website. "
#| "<https://krita.org/download/krita-desktop>`__\\ Click on 64bit or 32bit "
#| "according to the architecture of your OS. Go to the `KDE <http://download."
#| "kde.org/stable/krita/>`__ download directory to get the portable zip-file "
#| "version of Krita instead of the setup.exe installer."
msgid ""
"To download a portable version of Krita go to the `KDE <http://download.kde."
"org/stable/krita/>`__ download directory and get the zip-file instead of the "
"setup.exe installer."
msgstr ""
"Les utilisateurs de Windows peuvent télécharger la dernière version "
"disponible sur notre `site.  <https://krita.org/fr/telechargement/krita-"
"desktop/>`__\\ choisissez 64bit ou 32bit selon l'architecture de votre "
"système. Allez sur le dossier de téléchargement `KDE <https:// download.kde."
"org/stable/krita/>`__pour récupérer la version compacte du logiciel plutôt "
"que l'installeur setup.exe."

#: ../../user_manual/getting_started/installation.rst:48
msgid ""
"Krita requires Windows 7 or newer. The Store version requires Windows 10."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:51
msgid "Linux"
msgstr "Linux"

#: ../../user_manual/getting_started/installation.rst:53
#, fuzzy
#| msgid ""
#| "Many Linux distributions package the latest version of Krita. Sometimes "
#| "you will have to enable an extra repository. Krita runs fine under on "
#| "desktop: KDE, Gnome, LXDE -- even though it is a KDE SC application and "
#| "needs the KDE libraries. You might also want to install the KDE "
#| "systemsettings module and tweak the gui theme and fonts used, depending "
#| "on your distributions"
msgid ""
"Many Linux distributions package the latest version of Krita. Sometimes you "
"will have to enable an extra repository. Krita runs fine under most desktop "
"enviroments such as KDE, Gnome, LXDE, Xfce etc. -- even though it is a KDE "
"application and needs the KDE libraries. You might also want to install the "
"KDE system settings module and tweak the gui theme and fonts used, depending "
"on your distributions"
msgstr ""
"De nombreuses distributions packagent la dernière version de Krita. Parfois "
"vous devez activer des dépôts externes. Krita tourne bien avec les "
"gestionnaire de bureau : KDE, Gnome, LXDE -- quand bien même il s'agit d'une "
"application KDE SC et nécessite des bibliothèques KDE. Vous aurez "
"probablement besoin d'installer des modules de configuration système de KDE, "
"personnaliser le thème graphique et les fontes selon votre distribution"

#: ../../user_manual/getting_started/installation.rst:61
#, fuzzy
msgid "Nautilus/Nemo file extensions"
msgstr "Nautilus/Nemo extensions de fichier"

#: ../../user_manual/getting_started/installation.rst:63
#, fuzzy
#| msgid ""
#| "Since April 2016, KDE's Dolphin will show kra and ora thumbnails by "
#| "default, but Nautilus and it's derivatives will need an extension. `We "
#| "recommend Moritz Molch's extensions for XCF, KRA, ORA and PSD thumbnails "
#| "<http://moritzmolch.com/1749>`__."
msgid ""
"Since April 2016, KDE's Dolphin file manager shows kra and ora thumbnails by "
"default, but Nautilus and it's derivatives need an extension. `We recommend "
"Moritz Molch's extensions for XCF, KRA, ORA and PSD thumbnails <https://"
"moritzmolch.com/1749>`__."
msgstr ""
"Depuis avril 2016, Dolphin de KDE affiche les fichiers .kra et .ora par "
"défaut, mais Nautilus et ses dérivés ont besoin, pour cela, d'une extension. "
"Nous recommandons `celle de Moritz Molch pour afficher les vignettes des "
"fichiers XCF, ORA et PSD <http://moritzmolch.com/1749>`__."

#: ../../user_manual/getting_started/installation.rst:69
#, fuzzy
msgid "Appimages"
msgstr "Appimages"

#: ../../user_manual/getting_started/installation.rst:71
#, fuzzy
msgid ""
"For Krita 3.0 and later, first try out the appimage from the website. **90% "
"of the time this is by far the easiest way to get the latest Krita.** Just "
"download the appimage, and then use the file properties or the bash command "
"chmod to make the appimage executable. Double click it, and enjoy Krita. (Or "
"run it in the terminal with ./appimagename.appimage)"
msgstr ""
"Pour Krita 3.0 et suivant, essayez l'appimage que nous proposons. **90% du "
"temps c'est la façon la plus simple d'avoir la dernière version de Krita.** "
"Il suffit de télécharger l'appimage, et d'aller dans les propriété ou "
"d'utiliser la ligne de commande de bash chmod pour rendre l'appimage "
"exécutable. Double cliquez ensuite dessus et profitez de Krita. (Ou dans un "
"terminal : run ./appimagenom.appimage)"

#: ../../user_manual/getting_started/installation.rst:78
msgid "Open the terminal into the folder you have the appimage."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:79
msgid "Make it executable:"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:83
msgid "chmod a+x krita-3.0-x86_64.appimage"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:85
msgid "Run Krita!"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:89
msgid "./krita-3.0-x86_64.appimage"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:91
#, fuzzy
#| msgid ""
#| "Appimages are ISOs with all the necessary libraries inside, meaning no "
#| "fiddling with repositories and dependencies, at the cost of a slight bit "
#| "more diskspace taken up(And this size would only be bigger if you were "
#| "using Plasma to begin with)."
msgid ""
"Appimages are ISOs with all the necessary libraries bundled inside, that "
"means no fiddling with repositories and dependencies, at the cost of a "
"slight bit more diskspace taken up (And this size would only be bigger if "
"you were using Plasma to begin with)."
msgstr ""
"Appimage est un format d'application portable contenant toutes les "
"bibliothèques nécessaires, ce qui signifie qu'il n'est pas nécessaire "
"d'installer des dépôts et des dépendances, au prix d'un peu plus d'espace "
"disque occupé (et la taille est plus grande uniquement si vous utilisiez "
"Plasma)."

#: ../../user_manual/getting_started/installation.rst:97
#, fuzzy
msgid "Ubuntu and Kubuntu"
msgstr "Ubuntu et Kubuntu"

#: ../../user_manual/getting_started/installation.rst:99
#, fuzzy
msgid ""
"It does not matter which version of Ubuntu you use, Krita will run just "
"fine. However, by default, only a very old version of Krita is available. "
"You should either use the appimage, flatpak or the snap available from "
"Ubuntu's app store. We also maintain a ppa for getting latest builds of "
"Krita, you can read more about the ppa and install instructions `here "
"<https://launchpad.net/~kritalime/+archive/ubuntu/ppa>`_."
msgstr ""
"Peu importe la version d'ubuntu que vous utilisez, Krita fonctionnera très "
"bien. Cependant, par défaut, seule une ancienne version de Krita est "
"disponible. Vous devriez utiliser l'appimage, ou récupérer le snap sur la "
"logithèque Ubuntu."

#: ../../user_manual/getting_started/installation.rst:106
#, fuzzy
msgid "OpenSUSE"
msgstr "OpenSUSE"

#: ../../user_manual/getting_started/installation.rst:108
#, fuzzy
#| msgid "The most latest stable builds are available from KDE:Extra repo:"
msgid "The latest stable builds are available from KDE:Extra repo:"
msgstr ""
"Les dernières version stables sont disponible auprès de KDE:Extra repo:"

#: ../../user_manual/getting_started/installation.rst:110
#, fuzzy
msgid "https://download.opensuse.org/repositories/KDE:/Extra/"
msgstr "http://download.opensuse.org/repositories/KDE:/Extra/"

#: ../../user_manual/getting_started/installation.rst:113
#, fuzzy
#| msgid ""
#| "Krita is also in the official repos as *calligra-krita* , you can install "
#| "it from Yast."
msgid "Krita is also in the official repos, you can install it from Yast."
msgstr ""
"Krita est aussi dans le dépôt officiel connu sous le nom de *calligra-"
"krita*, vous pouvez l'installer depuis YaST."

#: ../../user_manual/getting_started/installation.rst:116
#, fuzzy
msgid "Fedora"
msgstr "Fedora"

#: ../../user_manual/getting_started/installation.rst:118
#, fuzzy
#| msgid ""
#| "Krita is in the official repos as **calligra-krita** , you can install it "
#| "by using packagekit (Add/Remove Software ) or by writing the command "
#| "below in terminal ``yum install calligra-krita`` Or ``dnf install "
#| "calligra-krita`` if you are using fedora 22."
msgid ""
"Krita is in the official repos, you can install it by using packagekit (Add/"
"Remove Software) or by writing the following command in terminal."
msgstr ""
"Krita est dans le dépôt officiel sous le nom de **calligra-krita**, vous "
"pouvez l'installer en utilisant packagekit (Installer/Supprimer Logiciel) ou "
"avec la commande depuis un terminal ``yum install calligra-krita`` ou ``dnf "
"install calligra-krita`` si vous utilisez fedora 22."

#: ../../user_manual/getting_started/installation.rst:120
msgid "``dnf install krita``"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:122
msgid ""
"You can also use the software center such as gnome software center or "
"Discover to install Krita."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:125
#, fuzzy
msgid "Debian"
msgstr "Debian"

#: ../../user_manual/getting_started/installation.rst:127
#, fuzzy
#| msgid ""
#| "The latest version of Krita available in Debian is 2.6. Jessie (stable) "
#| "has 2.8.5+dfsg-1+b2."
msgid ""
"The latest version of Krita available in Debian is 3.1.1. To install Krita "
"type the following line in terminal:"
msgstr ""
"La dernière version de Krita disponible pour Debian Jessie (version stable) "
"est 2.6  comme 2.8.5+dfsg-1+b2."

#: ../../user_manual/getting_started/installation.rst:130
msgid "``apt install krita``"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:134
msgid "Arch"
msgstr "Arch"

#: ../../user_manual/getting_started/installation.rst:136
msgid ""
"Arch Linux provides krita package in the Extra repository. You can install "
"Krita by using the following command:"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:139
msgid "``pacman -S krita``"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:141
msgid ""
"You can also find Krita pkgbuild in arch user repositories but it is not "
"guaranteed to contain the latest git version."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:145
#, fuzzy
msgid "OS X"
msgstr "OS X"

#: ../../user_manual/getting_started/installation.rst:147
#, fuzzy
msgid ""
"You can download the latest binary from our `website <https://krita.org/"
"download/krita-desktop/>`__. The binaries work only with Mac OSX version "
"10.12 and newer."
msgstr ""
"Vous pouvez télécharger la dernière version des binaires de Krita sur notre "
"site `internet <https://krita.org/download/krita-desktop/>`__. uniquement la "
"version pour Mac OSX 10.9 fonctionne."

#: ../../user_manual/getting_started/installation.rst:152
#, fuzzy
msgid "Source"
msgstr "Source"

#: ../../user_manual/getting_started/installation.rst:154
#, fuzzy
msgid ""
"While it is certainly more difficult to compile Krita from source than it is "
"to install from prebuilt packages, there are certain advantages that might "
"make the effort worth it:"
msgstr ""
"Bien qu'il soit certainement plus difficile de compiler Krita depuis les "
"sources que de l'installer depuis des paquets prédéfinis, il y a certains "
"avantages à le faire : "

#: ../../user_manual/getting_started/installation.rst:158
#, fuzzy
#| msgid ""
#| "You can follow the development of Krita on the foot. If you compile Krita "
#| "regularly from the development repository, you will be able to play "
#| "withall the new features that the developers are working on."
msgid ""
"You can follow the development of Krita on the foot. If you compile Krita "
"regularly from the development repository, you will be able to play with all "
"the new features that the developers are working on."
msgstr ""
"Vous pouvez suivre le développement de Krita pas à pas et compiler sa "
"version en développement. Vous serez en mesure de jouer pleinement avec les "
"nouvelles fonctionnalités sur lesquelles les développeurs travaillent."

#: ../../user_manual/getting_started/installation.rst:161
#, fuzzy
msgid ""
"You can compile it optimized for your processor. Most pre-built packages are "
"built for the lowest-common denominator."
msgstr ""
"Vous pouvez compiler afin d'optimiser Krita pour le processeur de votre "
"ordinateur. La plupart des packages prédéfinis sont fabriqué pour les plus "
"basses performances des ordinateurs."

#: ../../user_manual/getting_started/installation.rst:163
#, fuzzy
msgid "You will be getting all the bug fixes as soon as possible as well."
msgstr "Vous aurez les résolutions de bug les plus récentes."

#: ../../user_manual/getting_started/installation.rst:164
#, fuzzy
msgid ""
"You can help the developers by giving us your feedback on features as they "
"are being developed and you can test bug fixes for us. This is hugely "
"important, which is why our regular testers get their name in the about box "
"just like developers."
msgstr ""
"Vous pouvez aider les développeurs en leur rapportant votre avis sur les "
"fonctionnalités en cours de développement et vous pouvez tester les "
"corrections de bug pour nous. Ceci est extrêmement important, c'est pourquoi "
"nos testeurs réguliers ont leur nom dans le \"à propos\" comme nos "
"développeurs."

#: ../../user_manual/getting_started/installation.rst:169
#, fuzzy
#| msgid ""
#| "Of course, there are also disadvantages: when building from the current "
#| "development source repository you also get all the unfinished features. "
#| "It might mean less stability for a while, or things shown in the user "
#| "interface that don't work. But in practice, there seldom is really bad "
#| "instability, and if it is, it's easy for you to go back to a revision "
#| "that does work."
msgid ""
"Of course, there are also some disadvantages: when building from the current "
"development source repository you also get all the unfinished features. It "
"might mean less stability for a while, or things shown in the user interface "
"that don't work. But in practice, there is seldom really bad instability, "
"and if it is, it's easy for you to go back to a revision that does work."
msgstr ""
"Bien entendu, il y a également des désavantages : vous aurez également "
"affaire à des fonctionnalités non terminées, Krita sera moins stable "
"quelques temps, ou des boutons sur l'interface ne marcheront pas encore. "
"Mais, en pratique, l'instabilité est plutôt rare, et si c'est le cas, vous "
"pourrez toujours revenir à une version antérieure."

#: ../../user_manual/getting_started/installation.rst:176
#, fuzzy
msgid ""
"So... If you want to start compiling from source, begin with the latest "
"build instructions from the guide :ref:`here <building_krita>`."
msgstr ""
"Alors... si vous souhaitez compiler depuis les sources, commencez avec les "
"dernières instructions illustrées par le talentueux illustrateur dans le "
"`guide <http://www.davidrevoy.com/article193/guide-building-krita-on-linux-"
"for-cats>`__ de David Revoy."

#: ../../user_manual/getting_started/installation.rst:179
#, fuzzy
msgid ""
"If you encounter any problems, or if you are new to compiling software, "
"don't hesitate to contact the Krita developers. There are three main "
"communication channels:"
msgstr ""
"Si vous rencontrez des problèmes, ou si vous débutez en compilation de "
"logiciels, n'hésitez pas à contacter les développeurs Krita. Vous avez le "
"choix entre trois canaux principaux de communications:"

#: ../../user_manual/getting_started/installation.rst:183
#, fuzzy
msgid "irc: irc.freenode.net, channel #krita"
msgstr "irc: irc.freenode.net, channel #krita"

#: ../../user_manual/getting_started/installation.rst:184
#, fuzzy
msgid "`mailing list <https://mail.kde.org/mailman/listinfo/kimageshop>`__"
msgstr ""
"`liste de diffusion <https://mail.kde.org/mailman/listinfo/kimageshop>`__"

#: ../../user_manual/getting_started/installation.rst:185
#, fuzzy
msgid "`forums <https://forum.kde.org/viewforum.php?f=136>`__"
msgstr "`forums <http://forum.kde.org/viewforum.php?f=136>`__"

#, fuzzy
#~ msgid ""
#~ "Krita requires Windows Vista or newer. INTEL GRAPHICS CARD USERS: IF YOU "
#~ "SEE A BLACK OR BLANK WINDOW: UPDATE YOUR DRIVERS!"
#~ msgstr ""
#~ "Krita nécessite au moins Windows Vista ou des versions plus récentes de "
#~ "Windows. POUR LES UTILISATEURS DE CARTES GRAPHIQUES INTEL : SI UN ÉCRAN "
#~ "NOIR APPARAÎT OU UNE FENÊTRE NOIRE : METTEZ VOS DRIVERS À JOUR ! "

#, fuzzy
#~ msgid ""
#~ "Put here at the beginning, before we start on the many distro specific "
#~ "ways to get the program itself."
#~ msgstr ""
#~ "Mis en place dès le début, avant même de travailler sur Krita pour des "
#~ "distributions spécifiques."

#, fuzzy
#~ msgid ""
#~ "Mac OSX is very experimental right now and unstable, don't use it for "
#~ "production purpose."
#~ msgstr ""
#~ "Krita pour Mac OSX est très expérimental actuellement et instable. Nous "
#~ "vous déconseillons de l'utiliser pour un usage professionnel."

#, fuzzy
#~| msgid ""
#~| "There is more information and troubleshooting help on the `Calligra "
#~| "<https://community.kde.org/Calligra/Building>`__ wiki:"
#~ msgid ""
#~ "There is more information and troubleshooting help on the `Calligra "
#~ "<https://community.kde.org/Calligra/Building>`__ wiki."
#~ msgstr ""
#~ "Vous trouverez de plus amples informations et dépannages sur le `Calligra "
#~ "<https://community.kde.org/Calligra/Building>`__ wiki:"

#~ msgid ""
#~ "Arch Linux provides krita package in the Extra repository. You can "
#~ "install Krita by using the command below ``pacman -S krita`` You can "
#~ "install the most recent build of Krita using Yaourt repository with the "
#~ "help of command below ``yaourt -S krita-git``"
#~ msgstr ""
#~ "Arch Linux fournit le paquet Krita dans le dépôt Extra. Vous pouvez "
#~ "installer Krita en utilisant la commande ``pacman -S krita``Vous pouvez "
#~ "installer la version la plus récente de Krita en utilisant le dépôt "
#~ "Yaourt avec l'aide de la commande ``yaourt -S krita-git``"

#~ msgid ""
#~ ".. image:: images/icons/Krita_mouse_left.png\n"
#~ "   :alt: mouseleft"
#~ msgstr ""
#~ ".. image:: images/icons/Krita_mouse_left.png\n"
#~ "   :alt: clic gauche"

#~ msgid ""
#~ ".. image:: images/icons/Krita_mouse_right.png\n"
#~ "   :alt: mouseright"
#~ msgstr ""
#~ ".. image:: images/icons/Krita_mouse_right.png\n"
#~ "   :alt: clic droit"

#~ msgid ""
#~ ".. image:: images/icons/Krita_mouse_middle.png\n"
#~ "   :alt: mousemiddle"
#~ msgstr ""
#~ ".. image:: images/icons/Krita_mouse_middle.png\n"
#~ "   :alt: clic du milieu"

#~ msgid ""
#~ ".. image:: images/icons/Krita_mouse_scroll.png\n"
#~ "   :alt: mousescroll"
#~ msgstr ""
#~ ".. image:: images/icons/Krita_mouse_scroll.png\n"
#~ "   :alt: scroll"

#~ msgid ""
#~ ".. image:: images/icons/shape_select_tool.svg\n"
#~ "   :alt: toolshapeselection"
#~ msgstr ""
#~ ".. image:: images/icons/shape_select_tool.svg\n"
#~ "   :alt: outil sélection de forme"

#~ msgid ""
#~ ".. image:: images/icons/shape_edit_tool.svg\n"
#~ "   :alt: toolshapeedit"
#~ msgstr ""
#~ ".. image:: images/icons/shape_edit_tool.svg\n"
#~ "   :alt: outil modification de forme"

#~ msgid ""
#~ ".. image:: images/icons/text-tool.svg\n"
#~ "   :alt: tooltext"
#~ msgstr ""
#~ ".. image:: images/icons/text-tool.svg\n"
#~ "   :alt: outil texte"

#~ msgid ""
#~ ".. image:: images/icons/calligraphy_tool.svg\n"
#~ "   :alt: toolcalligraphy"
#~ msgstr ""
#~ ".. image:: images/icons/calligraphy_tool.svg\n"
#~ "   :alt: outil calligraphie"

#~ msgid ""
#~ ".. image:: images/icons/gradient_edit_tool.svg\n"
#~ "   :alt: toolgradientedit"
#~ msgstr ""
#~ ".. image:: images/icons/gradient_edit_tool.svg\n"
#~ "   :alt: outil modification dégradé"

#~ msgid ""
#~ ".. image:: images/icons/pattern_tool.svg\n"
#~ "   :alt: toolpatternedit"
#~ msgstr ""
#~ ".. image:: images/icons/pattern_tool.svg\n"
#~ "   :alt: outil modification motif"

#~ msgid ""
#~ ".. image:: images/icons/freehand_brush_tool.svg\n"
#~ "   :alt: toolfreehandbrush"
#~ msgstr ""
#~ ".. image:: images/icons/freehand_brush_tool.svg\n"
#~ "   :alt: brosse libre"

#~ msgid ""
#~ ".. image:: images/icons/line_tool.svg\n"
#~ "   :alt: toolline"
#~ msgstr ""
#~ ".. image:: images/icons/line_tool.svg\n"
#~ "   :alt: outil ligne"

#~ msgid ""
#~ ".. image:: images/icons/rectangle_tool.svg\n"
#~ "   :alt: toolrectangle"
#~ msgstr ""
#~ ".. image:: images/icons/rectangle_tool.svg\n"
#~ "   :alt: outil rectangle"

#~ msgid ""
#~ ".. image:: images/icons/ellipse_tool.svg\n"
#~ "   :alt: toolellipse"
#~ msgstr ""
#~ ".. image:: images/icons/ellipse_tool.svg\n"
#~ "   :alt: outil ellipse"

#~ msgid ""
#~ ".. image:: images/icons/polygon_tool.svg\n"
#~ "   :alt: toolpolygon"
#~ msgstr ""
#~ ".. image:: images/icons/polygon_tool.svg\n"
#~ "   :alt: outil polygone"

#~ msgid ""
#~ ".. image:: images/icons/polyline_tool.svg\n"
#~ "   :alt: toolpolyline"
#~ msgstr ""
#~ ".. image:: images/icons/polyline_tool.svg\n"
#~ "   :alt: outil polyligne"

#~ msgid ""
#~ ".. image:: images/icons/bezier_curve.svg\n"
#~ "   :alt: toolbeziercurve"
#~ msgstr ""
#~ ".. image:: images/icons/bezier_curve.svg\n"
#~ "   :alt: outil courbe de bézier"

#~ msgid ""
#~ ".. image:: images/icons/freehand_path_tool.svg\n"
#~ "   :alt: toolfreehandpath"
#~ msgstr ""
#~ ".. image:: images/icons/freehand_path_tool.svg\n"
#~ "   :alt: outil chemin libre "

#~ msgid ""
#~ ".. image:: images/icons/dyna_tool.svg\n"
#~ "   :alt: tooldyna"
#~ msgstr ""
#~ ".. image:: images/icons/dyna_tool.svg\n"
#~ "   :alt: outil dyna"

#~ msgid ""
#~ ".. image:: images/icons/multibrush_tool.svg\n"
#~ "   :alt: toolmultibrush"
#~ msgstr ""
#~ ".. image:: images/icons/multibrush_tool.svg\n"
#~ "   :alt: multibrosse"

#~ msgid ""
#~ ".. image:: images/icons/assistant_tool.svg\n"
#~ "   :alt: toolassistant"
#~ msgstr ""
#~ ".. image:: images/icons/assistant_tool.svg\n"
#~ "   :alt: assistant"

#~ msgid ""
#~ ".. image:: images/icons/move_tool.svg\n"
#~ "   :alt: toolmove"
#~ msgstr ""
#~ ".. image:: images/icons/move_tool.svg\n"
#~ "   :alt: outil déplacement"

#~ msgid ""
#~ ".. image:: images/icons/transform_tool.svg\n"
#~ "   :alt: tooltransform"
#~ msgstr ""
#~ ".. image:: images/icons/transform_tool.svg\n"
#~ "   :alt: transformation"

#~ msgid ""
#~ ".. image:: images/icons/grid_tool.svg\n"
#~ "   :alt: toolgrid"
#~ msgstr ""
#~ ".. image:: images/icons/grid_tool.svg\n"
#~ "   :alt: grille"

#~ msgid ""
#~ ".. image:: images/icons/perspectivegrid_tool.svg\n"
#~ "   :alt: toolperspectivegrid"
#~ msgstr ""
#~ ".. image:: images/icons/perspectivegrid_tool.svg\n"
#~ "   :alt: grille en perspective"

#~ msgid ""
#~ ".. image:: images/icons/measure_tool.svg\n"
#~ "   :alt: toolmeasure"
#~ msgstr ""
#~ ".. image:: images/icons/measure_tool.svg\n"
#~ "   :alt: outil mesure"

#~ msgid ""
#~ ".. image:: images/icons/color_picker_tool.svg\n"
#~ "   :alt: toolcolorpicker"
#~ msgstr ""
#~ ".. image:: images/icons/color_picker_tool.svg\n"
#~ "   :alt: outil pipette"

#~ msgid ""
#~ ".. image:: images/icons/fill_tool.svg\n"
#~ "   :alt: toolfill"
#~ msgstr ""
#~ ".. image:: images/icons/fill_tool.svg\n"
#~ "   :alt: remplissage"

#~ msgid ""
#~ ".. image:: images/icons/gradient_drawing_tool.svg\n"
#~ "   :alt: toolgradient"
#~ msgstr ""
#~ ".. image:: images/icons/gradient_drawing_tool.svg\n"
#~ "   :alt: dégradé"

#~ msgid ""
#~ ".. image:: images/icons/colorizemask_tool.svg\n"
#~ "   :alt: toolcolorizemask"
#~ msgstr ""
#~ ".. image:: images/icons/colorizemask_tool.svg\n"
#~ "   :alt: masque de colorisation"

#~ msgid ""
#~ ".. image:: images/icons/smart_patch_tool.svg\n"
#~ "   :alt: toolsmartpatch"
#~ msgstr ""
#~ ".. image:: images/icons/smart_patch_tool.svg\n"
#~ "   :alt: clonage intelligent"

#~ msgid ""
#~ ".. image:: images/icons/crop_tool.svg\n"
#~ "   :alt: toolcrop"
#~ msgstr ""
#~ ".. image:: images/icons/crop_tool.svg\n"
#~ "   :alt: découpe"

#~ msgid ""
#~ ".. image:: images/icons/rectangular_select_tool.svg\n"
#~ "   :alt: toolselectrect"
#~ msgstr ""
#~ ".. image:: images/icons/rectangular_select_tool.svg\n"
#~ "   :alt: sélection rectangulaire"

#~ msgid ""
#~ ".. image:: images/icons/elliptical_select_tool.svg\n"
#~ "   :alt: toolselectellipse"
#~ msgstr ""
#~ ".. image:: images/icons/elliptical_select_tool.svg\n"
#~ "   :alt: sélection elliptique"

#~ msgid ""
#~ ".. image:: images/icons/polygonal_select_tool.svg\n"
#~ "   :alt: toolselectpolygon"
#~ msgstr ""
#~ ".. image:: images/icons/polygonal_select_tool.svg\n"
#~ "   :alt: sélection polygonale"

#~ msgid ""
#~ ".. image:: images/icons/path_select_tool.svg\n"
#~ "   :alt: toolselectpath"
#~ msgstr ""
#~ ".. image:: images/icons/path_select_tool.svg\n"
#~ "   :alt: sélection des chemins"

#~ msgid ""
#~ ".. image:: images/icons/outline_select_tool.svg\n"
#~ "   :alt: toolselectoutline"
#~ msgstr ""
#~ ".. image:: images/icons/outline_select_tool.svg\n"
#~ "   :alt: sélection de contour"

#~ msgid ""
#~ ".. image:: images/icons/contiguous_select_tool.svg\n"
#~ "   :alt: toolselectcontiguous"
#~ msgstr ""
#~ ".. image:: images/icons/contiguous_select_tool.svg\n"
#~ "   :alt: sélection continue"

#~ msgid ""
#~ ".. image:: images/icons/similar_select_tool.svg\n"
#~ "   :alt: toolselectsimilar"
#~ msgstr ""
#~ ".. image:: images/icons/similar_select_tool.svg\n"
#~ "   :alt: sélection similaire"

#~ msgid ""
#~ ".. image:: images/icons/pan_tool.svg\n"
#~ "   :alt: toolpan"
#~ msgstr ""
#~ ".. image:: images/icons/pan_tool.svg\n"
#~ "   :alt: déplacement du canvas"

#~ msgid ""
#~ ".. image:: images/icons/zoom_tool.svg\n"
#~ "   :alt: toolzoom"
#~ msgstr ""
#~ ".. image:: images/icons/zoom_tool.svg\n"
#~ "   :alt: zoom"
