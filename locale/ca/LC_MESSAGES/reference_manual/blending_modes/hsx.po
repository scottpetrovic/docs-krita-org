# Translation of docs_krita_org_reference_manual___blending_modes___hsx.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 15:27+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/blending_modes/hsx.rst:1
msgid ""
"Page about the HSX blending modes in Krita, amongst which Hue, Color, "
"Luminosity and Saturation."
msgstr ""
"Pàgina sobre els modes de barreja HSX al Krita, entre els quals es troba To, "
"Color, Lluminositat i Saturació."

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:202
msgid "Intensity"
msgstr "Intensitat"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:214
msgid "Value"
msgstr "Valor"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:226
msgid "Lightness"
msgstr "Claredat"

#: ../../reference_manual/blending_modes/hsx.rst:11
#: ../../reference_manual/blending_modes/hsx.rst:238
msgid "Luminosity"
msgstr "Lluminositat"

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Hue"
msgstr "To"

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Saturation"
msgstr "Saturació"

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Luma"
msgstr "Luma"

#: ../../reference_manual/blending_modes/hsx.rst:11
msgid "Brightness"
msgstr "Brillantor"

#: ../../reference_manual/blending_modes/hsx.rst:15
msgid "HSX"
msgstr "HSX"

#: ../../reference_manual/blending_modes/hsx.rst:17
msgid ""
"Krita has four different HSX coordinate systems. The difference between them "
"is how they handle tone."
msgstr ""
"El Krita té quatre sistemes de coordenades HSX diferents. La diferència "
"entre ells és com manegen el to."

#: ../../reference_manual/blending_modes/hsx.rst:20
msgid "HSI"
msgstr "HSI"

#: ../../reference_manual/blending_modes/hsx.rst:22
msgid ""
"HSI is a color coordinate system, using Hue, Saturation and Intensity to "
"categorize a color. Hue is roughly the wavelength, whether the color is red, "
"yellow, green, cyan, blue or purple. It is measure in 360°, with 0 being "
"red. Saturation is the measurement of how close a color is to gray. "
"Intensity, in this case is the tone of the color. What makes intensity "
"special is that it recognizes yellow (rgb:1,1,0) having a higher combined "
"rgb value than blue (rgb:0,0,1). This is a non-linear tone dimension, which "
"means it's gamma-corrected."
msgstr ""
"El HSI és un sistema de coordenades del color, que utilitza el To, Saturació "
"i Intensitat per a categoritzar un color. El to és aproximadament la "
"longitud d'ona, si el color és vermell, groc, verd, cian, blau o porpra. Es "
"mesura en 360°, amb 0 de color vermell. La saturació és la mesura de la "
"proximitat del color gris. La intensitat, en aquest cas és el to del color. "
"El que fa que la intensitat sigui especial és que reconeix el groc "
"(rgb:1,1,0) que té un valor RGB combinat més gran que el blau (rgb:0,0,1). "
"Aquesta és una dimensió no lineal del to, el qual vol dir que es corregeix "
"la gamma."

#: ../../reference_manual/blending_modes/hsx.rst:28
msgid "HSL"
msgstr "HSL"

#: ../../reference_manual/blending_modes/hsx.rst:30
msgid ""
"HSL is also a color coordinate system. It describes colors in Hue, "
"Saturation and Lightness. Lightness specifically puts both yellow "
"(rgb:1,1,0), blue (rgb:0,0,1) and middle gray (rgb:0.5,0.5,0.5) at the same "
"lightness (0.5)."
msgstr ""
"El HSL també és un sistema de coordenades del color. Descriu els colors en "
"el To, Saturació i Claredat. La claredat posa específicament tant el groc "
"(rgb:1,1,0), el blau (rgb:0,0,1) i el gris mig (rgb:0,5,0,5,0,5) a la "
"mateixa claredat (0,5)."

#: ../../reference_manual/blending_modes/hsx.rst:34
msgid "HSV"
msgstr "HSV"

#: ../../reference_manual/blending_modes/hsx.rst:36
msgid ""
"HSV, occasionally called HSB, is a color coordinate system. It measures "
"colors in Hue, Saturation, and Value (also called Brightness). Value or "
"Brightness specifically refers to strength at which the pixel-lights on your "
"monitor have to shine. It sets Yellow (rgb:1,1,0), Blue (rgb:0,0,1) and "
"White (rgb:1,1,0) at the same Value (100%)"
msgstr ""
"El HSV, de vegades anomenat HSB, és un sistema de coordenades del color. "
"Mesura els colors en el To, Saturació i Valor (també anomenat brillantor). "
"El valor o la brillantor es refereix específicament a la força amb la qual "
"hauran de brillar els llums dels píxels en el vostre monitor. Estableix el "
"groc (rgb:1,1,0), blau (rgb:0,0,1) i blanc (rgb:1,1,0) al mateix valor "
"(100%)."

#: ../../reference_manual/blending_modes/hsx.rst:40
msgid "HSY"
msgstr "HSY"

#: ../../reference_manual/blending_modes/hsx.rst:42
msgid ""
"HSY is a color coordinate system. It categorizes colors in Hue, Saturation "
"and Luminosity. Well, not really, it uses Luma instead of true luminosity, "
"the difference being that Luminosity is linear while Luma is gamma-corrected "
"and just weights the rgb components. Luma is based on scientific studies of "
"how much light a color reflects in real-life. While like intensity it "
"acknowledges that yellow (rgb:1,1,0) is lighter than blue (rgb:0,0,1), it "
"also acknowledges that yellow (rgb:1,1,0) is lighter than cyan (rgb:0,1,1), "
"based on these studies."
msgstr ""
"El HSY és un sistema de coordenades del color. Categoritza els colors en el "
"To, Saturació i Lluminositat. Bé, no realment, fa servir la Luma "
"(luminància) en lloc d'una lluminositat veritable, la diferència és que la "
"lluminositat és lineal, mentre que la luma és corregida per la gamma i només "
"pesa els components RGB. La luma es basa en estudis científics sobre quanta "
"llum reflecteix un color a la vida real. Si bé té una intensitat similar, "
"reconeix que el groc (rgb:1,1,0) és més clar que el blau (rgb:0,0,1), també "
"reconeix que el groc (rgb:1,1,0) és més clar que el cian (rgb:0,1,1), a "
"partir d'aquests estudis."

#: ../../reference_manual/blending_modes/hsx.rst:46
msgid "HSX Blending Modes"
msgstr "Modes de barreja HSX"

#: ../../reference_manual/blending_modes/hsx.rst:55
msgid "Color, HSV, HSI, HSL, HSY"
msgstr "Color, HSV, HSI, HSL o HSY"

#: ../../reference_manual/blending_modes/hsx.rst:57
msgid ""
"This takes the Luminosity/Value/Intensity/Lightness of the colors on the "
"lower layer, and combines them with the Saturation and Hue of the upper "
"pixels. We refer to Color HSY as 'Color' in line with other applications."
msgstr ""
"Això pren la Lluminositat/Valor/Intensitat/Claredat dels colors en la capa "
"inferior i els combina amb la Saturació i el To dels píxels superiors. Ens "
"referirem al Color HSY com a «Color» en la línia com ho fan altres "
"aplicacions."

#: ../../reference_manual/blending_modes/hsx.rst:62
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/hsx.rst:62
#: ../../reference_manual/blending_modes/hsx.rst:67
#: ../../reference_manual/blending_modes/hsx.rst:72
msgid "Left: **Normal**. Right: **Color HSI**."
msgstr "Esquerra: **Normal**. Dreta: **Color HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:67
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/hsx.rst:72
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:78
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:78
msgid "Left: **Normal**. Right: **Color HSL**."
msgstr "Esquerra: **Normal**. Dreta: **Color HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:84
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:84
msgid "Left: **Normal**. Right: **Color HSV**."
msgstr "Esquerra: **Normal**. Dreta: **Color HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:90
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Color_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:90
msgid "Left: **Normal**. Right: **Color**."
msgstr "Esquerra: **Normal**. Dreta: **Color**."

#: ../../reference_manual/blending_modes/hsx.rst:99
msgid "Hue HSV, HSI, HSL, HSY"
msgstr "To HSV, HSI, HSL o HSY"

#: ../../reference_manual/blending_modes/hsx.rst:101
msgid ""
"Takes the saturation and tone of the lower layer and combines them with the "
"hue of the upper-layer. Tone in this case being either Value, Lightness, "
"Intensity or Luminosity."
msgstr ""
"Pren la saturació i el to de la capa inferior i els combina amb el to de la "
"capa superior. El to en aquest cas és Valor, Claredat, Intensitat o "
"Lluminositat."

#: ../../reference_manual/blending_modes/hsx.rst:107
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:107
msgid "Left: **Normal**. Right: **Hue HSI**."
msgstr "Esquerra: **Normal**. Dreta: **To HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:113
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:113
msgid "Left: **Normal**. Right: **Hue HSL**."
msgstr "Esquerra: **Normal**. Dreta: **To HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:119
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:119
msgid "Left: **Normal**. Right: **Hue HSV**."
msgstr "Esquerra: **Normal**. Dreta: **To HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:125
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Hue_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:125
msgid "Left: **Normal**. Right: **Hue**."
msgstr "Esquerra: **Normal**. Dreta: **To**."

#: ../../reference_manual/blending_modes/hsx.rst:134
msgid "Increase Value, Lightness, Intensity or Luminosity."
msgstr "Augmentar el valor, claredat, intensitat o lluminositat."

#: ../../reference_manual/blending_modes/hsx.rst:136
msgid ""
"Similar to lighten, but specific to tone. Checks whether the upper layer's "
"pixel has a higher tone than the lower layer's pixel. If so, the tone is "
"increased, if not, the lower layer's tone is maintained."
msgstr ""
"Similar a Aclarit, però específic al To. Comprova si el píxel de la capa "
"superior té un to més alt que el píxel de la capa inferior. Si és així, el "
"to augmentarà, de no ser així mantindrà el de la capa inferior."

#: ../../reference_manual/blending_modes/hsx.rst:142
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Intensity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Intensity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:142
msgid "Left: **Normal**. Right: **Increase Intensity**."
msgstr "Esquerra: **Normal**. Dreta: **Augmenta la intensitat**."

#: ../../reference_manual/blending_modes/hsx.rst:148
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Lightness_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Lightness_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:148
msgid "Left: **Normal**. Right: **Increase Lightness**."
msgstr "Esquerra: **Normal**. Dreta: **Augmenta la claredat**."

#: ../../reference_manual/blending_modes/hsx.rst:154
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Value_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Value_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:154
msgid "Left: **Normal**. Right: **Increase Value**."
msgstr "Esquerra: **Normal**. Dreta: **Augmenta el valor**."

#: ../../reference_manual/blending_modes/hsx.rst:160
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Luminosity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Luminosity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:160
msgid "Left: **Normal**. Right: **Increase Luminosity**."
msgstr "Esquerra: **Normal**. Dreta: **Augmenta la lluminositat**."

#: ../../reference_manual/blending_modes/hsx.rst:170
msgid "Increase Saturation HSI, HSV, HSL, HSY"
msgstr "Augmentar la saturació HSI, HSV, HSL o HSY"

#: ../../reference_manual/blending_modes/hsx.rst:172
msgid ""
"Similar to lighten, but specific to Saturation. Checks whether the upper "
"layer's pixel has a higher Saturation than the lower layer's pixel. If so, "
"the Saturation is increased, if not, the lower layer's Saturation is "
"maintained."
msgstr ""
"Similar a Aclarit, però específic a la Saturació. Comprova si el píxel de la "
"capa superior té una saturació més alta que el píxel de la capa inferior. Si "
"és així, la saturació augmentarà, de no ser així mantindrà la de la capa "
"inferior."

#: ../../reference_manual/blending_modes/hsx.rst:178
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:178
msgid "Left: **Normal**. Right: **Increase Saturation HSI**."
msgstr "Esquerra: **Normal**. Dreta: **Augmenta la saturació HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:184
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:184
msgid "Left: **Normal**. Right: **Increase Saturation HSL**."
msgstr "Esquerra: **Normal**. Dreta: **Augmenta la saturació HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:190
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:190
msgid "Left: **Normal**. Right: **Increase Saturation HSV**."
msgstr "Esquerra: **Normal**. Dreta: **Augmenta la saturació HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:196
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Increase_Saturation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:196
msgid "Left: **Normal**. Right: **Increase Saturation**."
msgstr "Esquerra: **Normal**. Dreta: **Augmenta la saturació**."

#: ../../reference_manual/blending_modes/hsx.rst:204
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"intensity of the upper layer."
msgstr ""
"Pren el To i la Saturació de la capa inferior i els emet amb la Intensitat "
"de la capa superior."

#: ../../reference_manual/blending_modes/hsx.rst:209
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Intensity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Intensity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:209
msgid "Left: **Normal**. Right: **Intensity**."
msgstr "Esquerra: **Normal**. Dreta: **Intensitat**."

#: ../../reference_manual/blending_modes/hsx.rst:216
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"Value of the upper layer."
msgstr ""
"Pren el To i la Saturació de la capa inferior i els emet amb el Valor de la "
"capa superior."

#: ../../reference_manual/blending_modes/hsx.rst:221
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Value_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Value_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:221
msgid "Left: **Normal**. Right: **Value**."
msgstr "Esquerra: **Normal**. Dreta: **Valor**."

#: ../../reference_manual/blending_modes/hsx.rst:228
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"Lightness of the upper layer."
msgstr ""
"Pren el To i la Saturació de la capa inferior i els emet amb la Claredat de "
"la capa superior."

#: ../../reference_manual/blending_modes/hsx.rst:233
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Lightness_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Lightness_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:233
msgid "Left: **Normal**. Right: **Lightness**."
msgstr "Esquerra: **Normal**. Dreta: **Claredat**."

#: ../../reference_manual/blending_modes/hsx.rst:240
msgid ""
"As explained above, actually Luma, but called this way as it's in line with "
"the terminology in other applications. Takes the Hue and Saturation of the "
"Lower layer and outputs them with the Luminosity of the upper layer. The "
"most preferred one of the four Tone blending modes, as this one gives fairly "
"intuitive results for the Tone of a hue"
msgstr ""
"Com es va explicar anteriorment, en realitat la Luma, però anomenada així "
"perquè s'alinea amb la terminologia en altres aplicacions. Pren el To i la "
"Saturació de la capa inferior i els emet amb la Lluminositat de la capa "
"superior. El més preferit dels quatre modes de barreja del to, ja que aquest "
"proporciona resultats bastant intuïtius per al To d'un matís."

#: ../../reference_manual/blending_modes/hsx.rst:247
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Luminosity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Luminosity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:247
msgid "Left: **Normal**. Right: **Luminosity**."
msgstr "Esquerra: **Normal**. Dreta: **Lluminositat**."

#: ../../reference_manual/blending_modes/hsx.rst:256
msgid "Saturation HSI, HSV, HSL, HSY"
msgstr "Saturació HSI, HSV, HSL o HSY"

#: ../../reference_manual/blending_modes/hsx.rst:258
msgid ""
"Takes the Intensity and Hue of the lower layer, and outputs them with the "
"HSI saturation of the upper layer."
msgstr ""
"Pren la Intensitat i el To de la capa inferior i els emet amb la Saturació "
"HSI de la capa superior."

#: ../../reference_manual/blending_modes/hsx.rst:263
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:263
msgid "Left: **Normal**. Right: **Saturation HSI**."
msgstr "Esquerra: **Normal**. Dreta: **Saturació HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:269
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:269
msgid "Left: **Normal**. Right: **Saturation HSL**."
msgstr "Esquerra: **Normal**. Dreta: **Saturació HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:275
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:275
msgid "Left: **Normal**. Right: **Saturation HSV**."
msgstr "Esquerra: **Normal**. Dreta: **Saturació HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:281
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Saturation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:281
msgid "Left: **Normal**. Right: **Saturation**."
msgstr "Esquerra: **Normal**. Dreta: **Saturació**."

#: ../../reference_manual/blending_modes/hsx.rst:289
msgid "Decrease Value, Lightness, Intensity or Luminosity"
msgstr "Disminuir el valor, claredat, intensitat o lluminositat"

#: ../../reference_manual/blending_modes/hsx.rst:291
msgid ""
"Similar to darken, but specific to tone. Checks whether the upper layer's "
"pixel has a lower tone than the lower layer's pixel. If so, the tone is "
"decreased, if not, the lower layer's tone is maintained."
msgstr ""
"Similar a Fosca, però específic al To. Comprova si el píxel de la capa "
"superior té un to més baix que el píxel de la capa inferior. Si és així, el "
"to disminuirà, de no ser així mantindrà el de la capa inferior."

#: ../../reference_manual/blending_modes/hsx.rst:297
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/hsx.rst:297
#: ../../reference_manual/blending_modes/hsx.rst:302
#: ../../reference_manual/blending_modes/hsx.rst:307
msgid "Left: **Normal**. Right: **Decrease Intensity**."
msgstr "Esquerra: **Normal**. Dreta: **Disminueix la intensitat**."

#: ../../reference_manual/blending_modes/hsx.rst:302
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/hsx.rst:307
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Intensity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:313
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Lightness_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Lightness_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:313
msgid "Left: **Normal**. Right: **Decrease Lightness**."
msgstr "Esquerra: **Normal**. Dreta: **Disminueix la claredat**."

#: ../../reference_manual/blending_modes/hsx.rst:319
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Value_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Value_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:319
msgid "Left: **Normal**. Right: **Decrease Value**."
msgstr "Esquerra: **Normal**. Dreta: **Disminueix el valor**."

#: ../../reference_manual/blending_modes/hsx.rst:325
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Luminosity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Luminosity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:325
msgid "Left: **Normal**. Right: **Decrease Luminosity**."
msgstr "Esquerra: **Normal**. Dreta: **Disminueix la lluminositat**."

#: ../../reference_manual/blending_modes/hsx.rst:334
msgid "Decrease Saturation HSI, HSV, HSL, HSY"
msgstr "Disminuir la saturació HSI, HSV, HSL o HSY"

#: ../../reference_manual/blending_modes/hsx.rst:336
msgid ""
"Similar to darken, but specific to Saturation. Checks whether the upper "
"layer's pixel has a lower Saturation than the lower layer's pixel. If so, "
"the Saturation is decreased, if not, the lower layer's Saturation is "
"maintained."
msgstr ""
"Similar a Fosca, però específic a la Saturació. Comprova si el píxel de la "
"capa superior té una saturació més baixa que el píxel de la capa inferior. "
"Si és així, la saturació disminuirà, de no ser així mantindrà la de la capa "
"inferior."

#: ../../reference_manual/blending_modes/hsx.rst:342
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/hsx.rst:342
#: ../../reference_manual/blending_modes/hsx.rst:347
#: ../../reference_manual/blending_modes/hsx.rst:352
msgid "Left: **Normal**. Right: **Decrease Saturation HSI**."
msgstr "Esquerra: **Normal**. Dreta: **Disminueix la saturació HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:347
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/hsx.rst:352
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:358
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:358
msgid "Left: **Normal**. Right: **Decrease Saturation HSL**."
msgstr "Esquerra: **Normal**. Dreta: **Disminueix la saturació HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:364
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:364
msgid "Left: **Normal**. Right: **Decrease Saturation HSV**."
msgstr "Esquerra: **Normal**. Dreta: **Disminueix la saturació HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:370
msgid ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/hsx/"
"Blending_modes_Decrease_Saturation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:370
msgid "Left: **Normal**. Right: **Decrease Saturation**."
msgstr "Esquerra: **Normal**. Dreta: **Disminueix la saturació**."
